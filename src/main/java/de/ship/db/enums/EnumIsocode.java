/*
 * This file is generated by jOOQ.
 */
package de.ship.db.enums;


import de.ship.db.CommonDd;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum EnumIsocode implements EnumType {

    de_DE("de_DE"),

    en("en"),

    pt_BR("pt_BR"),

    pl("pl");

    private final String literal;

    private EnumIsocode(String literal) {
        this.literal = literal;
    }

    @Override
    public Catalog getCatalog() {
        return getSchema().getCatalog();
    }

    @Override
    public Schema getSchema() {
        return CommonDd.COMMON_DD;
    }

    @Override
    public String getName() {
        return "enum_isocode";
    }

    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Lookup a value of this EnumType by its literal. Returns
     * <code>null</code>, if no such value could be found, see {@link
     * EnumType#lookupLiteral(Class, String)}.
     */
    public static EnumIsocode lookupLiteral(String literal) {
        return EnumType.lookupLiteral(EnumIsocode.class, literal);
    }
}
