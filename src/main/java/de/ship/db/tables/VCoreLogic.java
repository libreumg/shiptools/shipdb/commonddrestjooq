/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.enums.EnumLogicvalidreason;
import de.ship.db.tables.records.VCoreLogicRecord;

import java.time.LocalDateTime;
import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreLogic extends TableImpl<VCoreLogicRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_core_logic</code>
     */
    public static final VCoreLogic V_CORE_LOGIC = new VCoreLogic();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VCoreLogicRecord> getRecordType() {
        return VCoreLogicRecord.class;
    }

    /**
     * The column <code>common_dd.v_core_logic.key_element</code>.
     */
    public final TableField<VCoreLogicRecord, Integer> KEY_ELEMENT = createField(DSL.name("key_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_logic.logictype_descr</code>.
     */
    public final TableField<VCoreLogicRecord, String> LOGICTYPE_DESCR = createField(DSL.name("logictype_descr"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_logic.varname</code>.
     */
    public final TableField<VCoreLogicRecord, String> VARNAME = createField(DSL.name("varname"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_logic.condition</code>.
     */
    public final TableField<VCoreLogicRecord, String> CONDITION = createField(DSL.name("condition"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_logic.variable_stage</code>.
     */
    public final TableField<VCoreLogicRecord, Integer> VARIABLE_STAGE = createField(DSL.name("variable_stage"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_logic.varname_dn</code>.
     */
    public final TableField<VCoreLogicRecord, String> VARNAME_DN = createField(DSL.name("varname_dn"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_logic.condition_dn</code>.
     */
    public final TableField<VCoreLogicRecord, String> CONDITION_DN = createField(DSL.name("condition_dn"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_logic.valid_from</code>.
     */
    public final TableField<VCoreLogicRecord, LocalDateTime> VALID_FROM = createField(DSL.name("valid_from"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>common_dd.v_core_logic.valid_until</code>.
     */
    public final TableField<VCoreLogicRecord, LocalDateTime> VALID_UNTIL = createField(DSL.name("valid_until"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>common_dd.v_core_logic.active</code>.
     */
    public final TableField<VCoreLogicRecord, Boolean> ACTIVE = createField(DSL.name("active"), SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>common_dd.v_core_logic.valid_reason</code>.
     */
    public final TableField<VCoreLogicRecord, EnumLogicvalidreason> VALID_REASON = createField(DSL.name("valid_reason"), SQLDataType.VARCHAR.asEnumDataType(EnumLogicvalidreason.class), this, "");

    /**
     * The column <code>common_dd.v_core_logic.valid_comment</code>.
     */
    public final TableField<VCoreLogicRecord, String> VALID_COMMENT = createField(DSL.name("valid_comment"), SQLDataType.CLOB, this, "");

    private VCoreLogic(Name alias, Table<VCoreLogicRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VCoreLogic(Name alias, Table<VCoreLogicRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_core_logic" as  SELECT key_element,
          (logictype)::text AS logictype_descr,
          varname,
          (('('::text || string_agg(condition, ' ) or ( '::text ORDER BY condition)) || ')'::text) AS condition,
          variable_stage,
          varname_dn,
          (('('::text || string_agg(condition_dn, ' ) or ('::text ORDER BY condition_dn)) || ')'::text) AS condition_dn,
          valid_from,
          valid_until,
          active,
          valid_reason,
          valid_comment
         FROM ( SELECT v_1.varname,
                  v_1.orgroup,
                  v_1.logictype,
                  v_1.key_element,
                  string_agg(v_1.condition, ' and '::text ORDER BY v_1.condition) AS condition,
                  v_1.variable_stage,
                  v_1.varname_dn,
                  string_agg(v_1.condition_dn, ' and '::text ORDER BY v_1.condition_dn) AS condition_dn,
                  v_1.valid_from,
                  v_1.valid_until,
                  v_1.active,
                  v_1.valid_reason,
                  v_1.valid_comment
                 FROM ( SELECT ((((((eh.short_name || ' ['::text) || lc.left_depth) || '] '::text) || lc.comparator) || ' '::text) ||
                              CASE
                                  WHEN (lc.compare_variables = false) THEN
                                  CASE
                                      WHEN (lc.right_value IS NULL) THEN 'null'::text
                                      ELSE (''::text || rtrim(to_char(lc.right_value, 'FM9999999999999990D9999999999999999'::text), ','::text))
                                  END
                                  WHEN (lc.compare_variables = true) THEN (((COALESCE(eh3.short_name, 'ERROR'::text) || ' ['::text) || lc.right_depth) || ']'::text)
                                  ELSE '?'::text
                              END) AS condition,
                          ((((((eh.dn || ' ['::text) || lc.left_depth) || '] '::text) || lc.comparator) || ' '::text) ||
                              CASE
                                  WHEN (lc.compare_variables = false) THEN
                                  CASE
                                      WHEN (lc.right_value IS NULL) THEN 'null'::text
                                      ELSE (''::text || rtrim(to_char(lc.right_value, 'FM9999999999999990D9999999999999999'::text), ','::text))
                                  END
                                  WHEN (lc.compare_variables = true) THEN (((COALESCE(eh3.dn, 'ERROR'::text) || ' ['::text) || lc.right_depth) || ']'::text)
                                  ELSE '?'::text
                              END) AS condition_dn,
                          eh2.short_name AS varname,
                          eh2.dn AS varname_dn,
                          lc.fk_andgrp AS andgroup,
                          la.fk_orgrp AS orgroup,
                          l.logictype,
                          l.fk_element AS key_element,
                          eh2.stage AS variable_stage,
                          l.valid_from,
                          l.valid_until,
                          l.active,
                          l.valid_reason,
                          l.valid_comment
                         FROM (((((common_dd.v_core_logiccond lc
                           LEFT JOIN common_dd.t_core_element_hierarchy eh3 ON ((eh3.key_element = lc.fk_right_element)))
                           JOIN common_dd.t_core_element_hierarchy eh ON ((eh.key_element = lc.fk_left_element)))
                           JOIN common_dd.t_core_logicandgrp la ON ((la.andgrp = lc.fk_andgrp)))
                           JOIN common_dd.vm_core_logicchain l ON ((l.fk_orgrp = la.fk_orgrp)))
                           JOIN common_dd.t_core_element_hierarchy eh2 ON ((eh2.key_element = l.fk_element)))) v_1
                GROUP BY v_1.varname, v_1.andgroup, v_1.orgroup, v_1.logictype, v_1.key_element, v_1.variable_stage, v_1.varname_dn, v_1.valid_from, v_1.valid_until, v_1.active, v_1.valid_reason, v_1.valid_comment) v
        GROUP BY varname, orgroup, logictype, key_element, variable_stage, varname_dn, valid_from, valid_until, active, valid_reason, valid_comment;
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_core_logic</code> table reference
     */
    public VCoreLogic(String alias) {
        this(DSL.name(alias), V_CORE_LOGIC);
    }

    /**
     * Create an aliased <code>common_dd.v_core_logic</code> table reference
     */
    public VCoreLogic(Name alias) {
        this(alias, V_CORE_LOGIC);
    }

    /**
     * Create a <code>common_dd.v_core_logic</code> table reference
     */
    public VCoreLogic() {
        this(DSL.name("v_core_logic"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VCoreLogic as(String alias) {
        return new VCoreLogic(DSL.name(alias), this);
    }

    @Override
    public VCoreLogic as(Name alias) {
        return new VCoreLogic(alias, this);
    }

    @Override
    public VCoreLogic as(Table<?> alias) {
        return new VCoreLogic(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreLogic rename(String name) {
        return new VCoreLogic(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreLogic rename(Name name) {
        return new VCoreLogic(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreLogic rename(Table<?> name) {
        return new VCoreLogic(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreLogic where(Condition condition) {
        return new VCoreLogic(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreLogic where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreLogic where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreLogic where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreLogic where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreLogic where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreLogic where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreLogic where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreLogic whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreLogic whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
