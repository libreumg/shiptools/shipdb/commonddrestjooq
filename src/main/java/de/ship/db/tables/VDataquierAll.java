/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.enums.EnumDatatype;
import de.ship.db.tables.records.VDataquierAllRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.JSONB;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDataquierAll extends TableImpl<VDataquierAllRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_dataquier_all</code>
     */
    public static final VDataquierAll V_DATAQUIER_ALL = new VDataquierAll();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VDataquierAllRecord> getRecordType() {
        return VDataquierAllRecord.class;
    }

    /**
     * The column <code>common_dd.v_dataquier_all.var_names</code>.
     */
    public final TableField<VDataquierAllRecord, String> VAR_NAMES = createField(DSL.name("var_names"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.is_public</code>.
     */
    public final TableField<VDataquierAllRecord, Integer> IS_PUBLIC = createField(DSL.name("is_public"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stage</code>.
     */
    public final TableField<VDataquierAllRecord, Integer> STAGE = createField(DSL.name("stage"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.label</code>.
     */
    public final TableField<VDataquierAllRecord, String> LABEL = createField(DSL.name("label"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.long_label</code>.
     */
    public final TableField<VDataquierAllRecord, String> LONG_LABEL = createField(DSL.name("long_label"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.label_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> LABEL_DE = createField(DSL.name("label_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.long_label_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> LONG_LABEL_DE = createField(DSL.name("long_label_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.data_type</code>.
     */
    public final TableField<VDataquierAllRecord, EnumDatatype> DATA_TYPE = createField(DSL.name("data_type"), SQLDataType.VARCHAR.asEnumDataType(EnumDatatype.class), this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.value_labels</code>.
     */
    public final TableField<VDataquierAllRecord, String> VALUE_LABELS = createField(DSL.name("value_labels"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.value_labels_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> VALUE_LABELS_DE = createField(DSL.name("value_labels_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.missing_list</code>.
     */
    public final TableField<VDataquierAllRecord, String> MISSING_LIST = createField(DSL.name("missing_list"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.missing_list_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> MISSING_LIST_DE = createField(DSL.name("missing_list_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.key_observer</code>.
     */
    public final TableField<VDataquierAllRecord, String> KEY_OBSERVER = createField(DSL.name("key_observer"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.key_datetime</code>.
     */
    public final TableField<VDataquierAllRecord, String> KEY_DATETIME = createField(DSL.name("key_datetime"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.key_device</code>.
     */
    public final TableField<VDataquierAllRecord, String> KEY_DEVICE = createField(DSL.name("key_device"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.key_study_segment</code>.
     */
    public final TableField<VDataquierAllRecord, String> KEY_STUDY_SEGMENT = createField(DSL.name("key_study_segment"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.variable_order</code>.
     */
    public final TableField<VDataquierAllRecord, Integer> VARIABLE_ORDER = createField(DSL.name("variable_order"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.short_name</code>.
     */
    public final TableField<VDataquierAllRecord, String> SHORT_NAME = createField(DSL.name("short_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_missing</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_MISSING = createField(DSL.name("stata_missing"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_dependency</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_DEPENDENCY = createField(DSL.name("stata_dependency"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_hard_limit</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_HARD_LIMIT = createField(DSL.name("stata_hard_limit"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_soft_limit</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_SOFT_LIMIT = createField(DSL.name("stata_soft_limit"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_key_observer</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_KEY_OBSERVER = createField(DSL.name("stata_key_observer"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_key_datetime</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_KEY_DATETIME = createField(DSL.name("stata_key_datetime"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_key_device</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_KEY_DEVICE = createField(DSL.name("stata_key_device"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.hard_limits</code>.
     */
    public final TableField<VDataquierAllRecord, JSONB> HARD_LIMITS = createField(DSL.name("hard_limits"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.soft_limits</code>.
     */
    public final TableField<VDataquierAllRecord, JSONB> SOFT_LIMITS = createField(DSL.name("soft_limits"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.jump_list</code>.
     */
    public final TableField<VDataquierAllRecord, JSONB> JUMP_LIST = createField(DSL.name("jump_list"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.report_name</code>.
     */
    public final TableField<VDataquierAllRecord, String> REPORT_NAME = createField(DSL.name("report_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.create_report</code>.
     */
    public final TableField<VDataquierAllRecord, String> CREATE_REPORT = createField(DSL.name("create_report"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.stata_jumps</code>.
     */
    public final TableField<VDataquierAllRecord, String> STATA_JUMPS = createField(DSL.name("stata_jumps"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.jump_list_en</code>.
     */
    public final TableField<VDataquierAllRecord, String> JUMP_LIST_EN = createField(DSL.name("jump_list_en"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.jump_list_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> JUMP_LIST_DE = createField(DSL.name("jump_list_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.variable_role</code>.
     */
    public final TableField<VDataquierAllRecord, String> VARIABLE_ROLE = createField(DSL.name("variable_role"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.order_nr</code>.
     */
    public final TableField<VDataquierAllRecord, Integer> ORDER_NR = createField(DSL.name("order_nr"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.parent_name</code>.
     */
    public final TableField<VDataquierAllRecord, String> PARENT_NAME = createField(DSL.name("parent_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.table_name</code>.
     */
    public final TableField<VDataquierAllRecord, String> TABLE_NAME = createField(DSL.name("table_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.qs_missing_list_en</code>.
     */
    public final TableField<VDataquierAllRecord, String> QS_MISSING_LIST_EN = createField(DSL.name("qs_missing_list_en"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.qs_missing_list_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> QS_MISSING_LIST_DE = createField(DSL.name("qs_missing_list_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.qs_jump_list_en</code>.
     */
    public final TableField<VDataquierAllRecord, String> QS_JUMP_LIST_EN = createField(DSL.name("qs_jump_list_en"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.qs_jump_list_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> QS_JUMP_LIST_DE = createField(DSL.name("qs_jump_list_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.note_de</code>.
     */
    public final TableField<VDataquierAllRecord, String> NOTE_DE = createField(DSL.name("note_de"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.note_en</code>.
     */
    public final TableField<VDataquierAllRecord, String> NOTE_EN = createField(DSL.name("note_en"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dataquier_all.maelstrom_annotation</code>.
     */
    public final TableField<VDataquierAllRecord, JSONB> MAELSTROM_ANNOTATION = createField(DSL.name("maelstrom_annotation"), SQLDataType.JSONB, this, "");

    private VDataquierAll(Name alias, Table<VDataquierAllRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VDataquierAll(Name alias, Table<VDataquierAllRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_dataquier_all" as  WITH attr(fk_element, hard_limits, soft_limits, jump_list) AS (
                SELECT v_1.key_variable,
                   h.value,
                   s.value,
                   j.value
                  FROM (((common_dd.t_core_variable v_1
                    LEFT JOIN common_dd.t_core_variableattr h ON (((h.fk_variable = v_1.key_variable) AND (h.fk_variableattrtype IN ( SELECT t_core_variableattrtype.pk
                          FROM common_dd.t_core_variableattrtype
                         WHERE (t_core_variableattrtype.name = 'hard_limits'::text))))))
                    LEFT JOIN common_dd.t_core_variableattr s ON (((s.fk_variable = v_1.key_variable) AND (s.fk_variableattrtype IN ( SELECT t_core_variableattrtype.pk
                          FROM common_dd.t_core_variableattrtype
                         WHERE (t_core_variableattrtype.name = 'soft_limits'::text))))))
                    LEFT JOIN common_dd.t_core_variableattr j ON (((j.fk_variable = v_1.key_variable) AND (j.fk_variableattrtype IN ( SELECT t_core_variableattrtype.pk
                          FROM common_dd.t_core_variableattrtype
                         WHERE (t_core_variableattrtype.name = 'jump_list'::text))))))
               ), ml(list, isocode, missingtype, codes) AS (
                SELECT v_dataquier_missinglist.list,
                   v_dataquier_missinglist.isocode,
                   v_dataquier_missinglist.missingtype,
                   v_dataquier_missinglist.codes
                  FROM common_dd.v_dataquier_missinglist
               ), ml_qs(list, isocode, missingtype, codes, stata_codes) AS (
                SELECT v_dataquier_missinglist_qs.list,
                   v_dataquier_missinglist_qs.isocode,
                   v_dataquier_missinglist_qs.missingtype,
                   v_dataquier_missinglist_qs.codes,
                   v_dataquier_missinglist_qs.stata_codes
                  FROM common_dd.v_dataquier_missinglist_qs
               ), vdv(fk_valuelist, isocode, codes) AS (
                SELECT v_dataquier_valuelist.fk_valuelist,
                   v_dataquier_valuelist.isocode,
                   v_dataquier_valuelist.codes
                  FROM common_dd.v_dataquier_valuelist
               )
        SELECT e.unique_name AS var_names,
           e.is_public,
           e.stage,
           t1.value AS label,
           t2.value AS long_label,
           t3.value AS label_de,
           t4.value AS long_label_de,
           v.data_type,
           vl_en.codes AS value_labels,
           vl_de.codes AS value_labels_de,
           ml_en.codes AS missing_list,
           ml_de.codes AS missing_list_de,
           pe1.unique_name AS key_observer,
           pe2.unique_name AS key_datetime,
           pe3.unique_name AS key_device,
           seg.name AS key_study_segment,
           e.order_nr AS variable_order,
           e.short_name,
           qml_en.stata_codes AS stata_missing,
           lo.dependency AS stata_dependency,
           lo.hardlimit AS stata_hard_limit,
           lo.softlimit AS stata_soft_limit,
           pe1.short_name AS stata_key_observer,
           pe2.short_name AS stata_key_datetime,
           pe3.short_name AS stata_key_device,
           a.hard_limits,
           a.soft_limits,
           a.jump_list,
           seg.report_name,
           seg.create_report,
           qjl_en.stata_codes AS stata_jumps,
           jl_en.codes AS jump_list_en,
           jl_de.codes AS jump_list_de,
           seg.variable_role,
           e.order_nr,
           COALESCE(p.unique_name, p.dn) AS parent_name,
           upper(('t_'::text || COALESCE(tabledef.table_short_name, 'unspecified'::text))) AS table_name,
           qml_en.codes AS qs_missing_list_en,
           qml_de.codes AS qs_missing_list_de,
           qjl_en.codes AS qs_jump_list_en,
           qjl_de.codes AS qs_jump_list_de,
           n.note_de,
           n.note_en,
           ma.annotation AS maelstrom_annotation
          FROM (((((((((((((((((((((((((((((((common_dd.t_core_variable v
            LEFT JOIN attr a ON ((a.fk_element = v.key_variable)))
            LEFT JOIN common_dd.t_core_variable_segment seg ON ((seg.fk_element = v.key_variable)))
            LEFT JOIN common_dd.t_core_element_hierarchy e ON ((e.key_element = v.key_variable)))
            LEFT JOIN common_dd.t_core_element_hierarchy p ON ((p.key_element = e.key_parent_element)))
            LEFT JOIN common_dd.t_core_translation t1 ON (((t1.fk_trans_key = e.hier_lang) AND (t1.isocode = 'en'::common_dd.enum_isocode) AND (t1.category = 'crf'::common_dd.enum_trans_category))))
            LEFT JOIN common_dd.t_core_translation t2 ON (((t2.fk_trans_key = e.hier_lang) AND (t2.isocode = 'en'::common_dd.enum_isocode) AND (t2.category = 'long'::common_dd.enum_trans_category))))
            LEFT JOIN common_dd.t_core_translation t3 ON (((t3.fk_trans_key = e.hier_lang) AND (t3.isocode = 'de_DE'::common_dd.enum_isocode) AND (t3.category = 'crf'::common_dd.enum_trans_category))))
            LEFT JOIN common_dd.t_core_translation t4 ON (((t4.fk_trans_key = e.hier_lang) AND (t4.isocode = 'de_DE'::common_dd.enum_isocode) AND (t4.category = 'long'::common_dd.enum_trans_category))))
            LEFT JOIN common_dd.t_core_processtype pt1 ON ((pt1.name = 'observer'::text)))
            LEFT JOIN common_dd.t_core_processvar pv1 ON (((pv1.fk_element = e.key_element) AND (pv1.fk_processtype = pt1.pk))))
            LEFT JOIN common_dd.t_core_element_hierarchy pe1 ON ((pe1.key_element = pv1.fk_referenced_element)))
            LEFT JOIN common_dd.t_core_processtype pt2 ON ((pt2.name = 'timevar'::text)))
            LEFT JOIN common_dd.t_core_processvar pv2 ON (((pv2.fk_element = e.key_element) AND (pv2.fk_processtype = pt2.pk))))
            LEFT JOIN common_dd.t_core_element_hierarchy pe2 ON ((pe2.key_element = pv2.fk_referenced_element)))
            LEFT JOIN common_dd.t_core_processtype pt3 ON ((pt3.name = 'device'::text)))
            LEFT JOIN common_dd.t_core_processvar pv3 ON (((pv3.fk_element = e.key_element) AND (pv3.fk_processtype = pt3.pk))))
            LEFT JOIN common_dd.t_core_element_hierarchy pe3 ON ((pe3.key_element = pv3.fk_referenced_element)))
            LEFT JOIN vdv vl_en ON (((vl_en.fk_valuelist = v.fk_var_valuelist) AND (vl_en.isocode = 'en'::common_dd.enum_isocode))))
            LEFT JOIN vdv vl_de ON (((vl_de.fk_valuelist = v.fk_var_valuelist) AND (vl_de.isocode = 'de_DE'::common_dd.enum_isocode))))
            LEFT JOIN ml ml_en ON (((ml_en.list = v.fk_missinglist) AND (ml_en.isocode = 'en'::common_dd.enum_isocode) AND (ml_en.missingtype = 'missing'::text))))
            LEFT JOIN ml ml_de ON (((ml_de.list = v.fk_missinglist) AND (ml_de.isocode = 'de_DE'::common_dd.enum_isocode) AND (ml_de.missingtype = 'missing'::text))))
            LEFT JOIN ml jl_en ON (((jl_en.list = v.fk_missinglist) AND (jl_en.isocode = 'en'::common_dd.enum_isocode) AND (jl_en.missingtype = 'jump'::text))))
            LEFT JOIN ml jl_de ON (((jl_de.list = v.fk_missinglist) AND (jl_de.isocode = 'de_DE'::common_dd.enum_isocode) AND (jl_de.missingtype = 'jump'::text))))
            LEFT JOIN ml_qs qml_en ON (((qml_en.list = v.fk_missinglist) AND (qml_en.isocode = 'en'::common_dd.enum_isocode) AND (qml_en.missingtype = 'missing'::text))))
            LEFT JOIN ml_qs qml_de ON (((qml_de.list = v.fk_missinglist) AND (qml_de.isocode = 'de_DE'::common_dd.enum_isocode) AND (qml_de.missingtype = 'missing'::text))))
            LEFT JOIN ml_qs qjl_en ON (((qjl_en.list = v.fk_missinglist) AND (qjl_en.isocode = 'en'::common_dd.enum_isocode) AND (qjl_en.missingtype = 'jump'::text))))
            LEFT JOIN ml_qs qjl_de ON (((qjl_de.list = v.fk_missinglist) AND (qjl_de.isocode = 'de_DE'::common_dd.enum_isocode) AND (qjl_de.missingtype = 'jump'::text))))
            LEFT JOIN common_dd.v_dataquier_logic lo ON ((lo.fk_element = v.key_variable)))
            LEFT JOIN common_dd.v_dataquier_notes n ON ((n.fk_element = e.key_element)))
            LEFT JOIN common_dd.v_dm_tablevar tabledef ON ((tabledef.variable_key_element = v.key_variable)))
            LEFT JOIN common_dd.v_maelstrom_annotation ma ON ((ma.unique_name = e.unique_name)))
         WHERE (e.unique_name IS NOT NULL);
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_dataquier_all</code> table reference
     */
    public VDataquierAll(String alias) {
        this(DSL.name(alias), V_DATAQUIER_ALL);
    }

    /**
     * Create an aliased <code>common_dd.v_dataquier_all</code> table reference
     */
    public VDataquierAll(Name alias) {
        this(alias, V_DATAQUIER_ALL);
    }

    /**
     * Create a <code>common_dd.v_dataquier_all</code> table reference
     */
    public VDataquierAll() {
        this(DSL.name("v_dataquier_all"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VDataquierAll as(String alias) {
        return new VDataquierAll(DSL.name(alias), this);
    }

    @Override
    public VDataquierAll as(Name alias) {
        return new VDataquierAll(alias, this);
    }

    @Override
    public VDataquierAll as(Table<?> alias) {
        return new VDataquierAll(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VDataquierAll rename(String name) {
        return new VDataquierAll(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDataquierAll rename(Name name) {
        return new VDataquierAll(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDataquierAll rename(Table<?> name) {
        return new VDataquierAll(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDataquierAll where(Condition condition) {
        return new VDataquierAll(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDataquierAll where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDataquierAll where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDataquierAll where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDataquierAll where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDataquierAll where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDataquierAll where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDataquierAll where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDataquierAll whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDataquierAll whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
