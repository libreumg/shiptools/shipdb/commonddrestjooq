/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VCoreValuelistRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.JSONB;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreValuelist extends TableImpl<VCoreValuelistRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_core_valuelist</code>
     */
    public static final VCoreValuelist V_CORE_VALUELIST = new VCoreValuelist();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VCoreValuelistRecord> getRecordType() {
        return VCoreValuelistRecord.class;
    }

    /**
     * The column <code>common_dd.v_core_valuelist.valuelist</code>.
     */
    public final TableField<VCoreValuelistRecord, Integer> VALUELIST = createField(DSL.name("valuelist"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_valuelist.key</code>.
     */
    public final TableField<VCoreValuelistRecord, String> KEY = createField(DSL.name("key"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_valuelist.value</code>.
     */
    public final TableField<VCoreValuelistRecord, JSONB> VALUE = createField(DSL.name("value"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_core_valuelist.globallist</code>.
     */
    public final TableField<VCoreValuelistRecord, Boolean> GLOBALLIST = createField(DSL.name("globallist"), SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>common_dd.v_core_valuelist.changeable</code>.
     */
    public final TableField<VCoreValuelistRecord, Boolean> CHANGEABLE = createField(DSL.name("changeable"), SQLDataType.BOOLEAN, this, "");

    private VCoreValuelist(Name alias, Table<VCoreValuelistRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VCoreValuelist(Name alias, Table<VCoreValuelistRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_core_valuelist" as  SELECT t_core_valuelist.valuelist,
          json_data.key,
          json_data.value,
          t_core_valuelist.globallist,
          t_core_valuelist.changeable
         FROM common_dd.t_core_valuelist,
          LATERAL jsonb_each(t_core_valuelist."values") json_data(key, value)
        WHERE (t_core_valuelist.fk_valuetype = 1);
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_core_valuelist</code> table reference
     */
    public VCoreValuelist(String alias) {
        this(DSL.name(alias), V_CORE_VALUELIST);
    }

    /**
     * Create an aliased <code>common_dd.v_core_valuelist</code> table reference
     */
    public VCoreValuelist(Name alias) {
        this(alias, V_CORE_VALUELIST);
    }

    /**
     * Create a <code>common_dd.v_core_valuelist</code> table reference
     */
    public VCoreValuelist() {
        this(DSL.name("v_core_valuelist"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VCoreValuelist as(String alias) {
        return new VCoreValuelist(DSL.name(alias), this);
    }

    @Override
    public VCoreValuelist as(Name alias) {
        return new VCoreValuelist(alias, this);
    }

    @Override
    public VCoreValuelist as(Table<?> alias) {
        return new VCoreValuelist(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreValuelist rename(String name) {
        return new VCoreValuelist(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreValuelist rename(Name name) {
        return new VCoreValuelist(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreValuelist rename(Table<?> name) {
        return new VCoreValuelist(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreValuelist where(Condition condition) {
        return new VCoreValuelist(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreValuelist where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreValuelist where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreValuelist where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreValuelist where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreValuelist where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreValuelist where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreValuelist where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreValuelist whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreValuelist whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
