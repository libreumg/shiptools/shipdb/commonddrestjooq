/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.Keys;
import de.ship.db.enums.EnumCompleteness;
import de.ship.db.enums.EnumQuality;
import de.ship.db.tables.TCoreElementHierarchy.TCoreElementHierarchyPath;
import de.ship.db.tables.records.TCoreElementpublicRecord;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.InverseForeignKey;
import org.jooq.Name;
import org.jooq.Path;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.Record;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreElementpublic extends TableImpl<TCoreElementpublicRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.t_core_elementpublic</code>
     */
    public static final TCoreElementpublic T_CORE_ELEMENTPUBLIC = new TCoreElementpublic();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TCoreElementpublicRecord> getRecordType() {
        return TCoreElementpublicRecord.class;
    }

    /**
     * The column <code>common_dd.t_core_elementpublic.fk_element</code>.
     */
    public final TableField<TCoreElementpublicRecord, Integer> FK_ELEMENT = createField(DSL.name("fk_element"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>common_dd.t_core_elementpublic.is_public</code>.
     */
    public final TableField<TCoreElementpublicRecord, Boolean> IS_PUBLIC = createField(DSL.name("is_public"), SQLDataType.BOOLEAN.nullable(false), this, "");

    /**
     * The column <code>common_dd.t_core_elementpublic.quality</code>.
     */
    public final TableField<TCoreElementpublicRecord, EnumQuality> QUALITY = createField(DSL.name("quality"), SQLDataType.VARCHAR.asEnumDataType(EnumQuality.class), this, "");

    /**
     * The column <code>common_dd.t_core_elementpublic.completeness</code>.
     */
    public final TableField<TCoreElementpublicRecord, EnumCompleteness> COMPLETENESS = createField(DSL.name("completeness"), SQLDataType.VARCHAR.asEnumDataType(EnumCompleteness.class), this, "");

    private TCoreElementpublic(Name alias, Table<TCoreElementpublicRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private TCoreElementpublic(Name alias, Table<TCoreElementpublicRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table(), where);
    }

    /**
     * Create an aliased <code>common_dd.t_core_elementpublic</code> table
     * reference
     */
    public TCoreElementpublic(String alias) {
        this(DSL.name(alias), T_CORE_ELEMENTPUBLIC);
    }

    /**
     * Create an aliased <code>common_dd.t_core_elementpublic</code> table
     * reference
     */
    public TCoreElementpublic(Name alias) {
        this(alias, T_CORE_ELEMENTPUBLIC);
    }

    /**
     * Create a <code>common_dd.t_core_elementpublic</code> table reference
     */
    public TCoreElementpublic() {
        this(DSL.name("t_core_elementpublic"), null);
    }

    public <O extends Record> TCoreElementpublic(Table<O> path, ForeignKey<O, TCoreElementpublicRecord> childPath, InverseForeignKey<O, TCoreElementpublicRecord> parentPath) {
        super(path, childPath, parentPath, T_CORE_ELEMENTPUBLIC);
    }

    /**
     * A subtype implementing {@link Path} for simplified path-based joins.
     */
    public static class TCoreElementpublicPath extends TCoreElementpublic implements Path<TCoreElementpublicRecord> {

        private static final long serialVersionUID = 1L;
        public <O extends Record> TCoreElementpublicPath(Table<O> path, ForeignKey<O, TCoreElementpublicRecord> childPath, InverseForeignKey<O, TCoreElementpublicRecord> parentPath) {
            super(path, childPath, parentPath);
        }
        private TCoreElementpublicPath(Name alias, Table<TCoreElementpublicRecord> aliased) {
            super(alias, aliased);
        }

        @Override
        public TCoreElementpublicPath as(String alias) {
            return new TCoreElementpublicPath(DSL.name(alias), this);
        }

        @Override
        public TCoreElementpublicPath as(Name alias) {
            return new TCoreElementpublicPath(alias, this);
        }

        @Override
        public TCoreElementpublicPath as(Table<?> alias) {
            return new TCoreElementpublicPath(alias.getQualifiedName(), this);
        }
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public UniqueKey<TCoreElementpublicRecord> getPrimaryKey() {
        return Keys.T_CORE_ELEMENTPUBLIC_PKEY;
    }

    @Override
    public List<ForeignKey<TCoreElementpublicRecord, ?>> getReferences() {
        return Arrays.asList(Keys.T_CORE_ELEMENTPUBLIC__T_CORE_ELEMENTPUBLIC_FK_ELEMENT_FKEY);
    }

    private transient TCoreElementHierarchyPath _tCoreElementHierarchy;

    /**
     * Get the implicit join path to the
     * <code>common_dd.t_core_element_hierarchy</code> table.
     */
    public TCoreElementHierarchyPath tCoreElementHierarchy() {
        if (_tCoreElementHierarchy == null)
            _tCoreElementHierarchy = new TCoreElementHierarchyPath(this, Keys.T_CORE_ELEMENTPUBLIC__T_CORE_ELEMENTPUBLIC_FK_ELEMENT_FKEY, null);

        return _tCoreElementHierarchy;
    }

    @Override
    public TCoreElementpublic as(String alias) {
        return new TCoreElementpublic(DSL.name(alias), this);
    }

    @Override
    public TCoreElementpublic as(Name alias) {
        return new TCoreElementpublic(alias, this);
    }

    @Override
    public TCoreElementpublic as(Table<?> alias) {
        return new TCoreElementpublic(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreElementpublic rename(String name) {
        return new TCoreElementpublic(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreElementpublic rename(Name name) {
        return new TCoreElementpublic(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreElementpublic rename(Table<?> name) {
        return new TCoreElementpublic(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementpublic where(Condition condition) {
        return new TCoreElementpublic(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementpublic where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementpublic where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementpublic where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementpublic where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementpublic where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementpublic where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementpublic where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementpublic whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementpublic whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
