/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VDmTablevarRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDmTablevar extends TableImpl<VDmTablevarRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_dm_tablevar</code>
     */
    public static final VDmTablevar V_DM_TABLEVAR = new VDmTablevar();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VDmTablevarRecord> getRecordType() {
        return VDmTablevarRecord.class;
    }

    /**
     * The column <code>common_dd.v_dm_tablevar.variable_key_element</code>.
     */
    public final TableField<VDmTablevarRecord, Integer> VARIABLE_KEY_ELEMENT = createField(DSL.name("variable_key_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_tablevar.variable_dn</code>.
     */
    public final TableField<VDmTablevarRecord, String> VARIABLE_DN = createField(DSL.name("variable_dn"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_tablevar.variable_short_name</code>.
     */
    public final TableField<VDmTablevarRecord, String> VARIABLE_SHORT_NAME = createField(DSL.name("variable_short_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_tablevar.table_key_element</code>.
     */
    public final TableField<VDmTablevarRecord, Integer> TABLE_KEY_ELEMENT = createField(DSL.name("table_key_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_tablevar.table_dn</code>.
     */
    public final TableField<VDmTablevarRecord, String> TABLE_DN = createField(DSL.name("table_dn"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_tablevar.table_short_name</code>.
     */
    public final TableField<VDmTablevarRecord, String> TABLE_SHORT_NAME = createField(DSL.name("table_short_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_tablevar.study_name</code>.
     */
    public final TableField<VDmTablevarRecord, String> STUDY_NAME = createField(DSL.name("study_name"), SQLDataType.CLOB, this, "");

    private VDmTablevar(Name alias, Table<VDmTablevarRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VDmTablevar(Name alias, Table<VDmTablevarRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_dm_tablevar" as  WITH RECURSIVE r(k, p, td) AS (
                SELECT t_core_element_hierarchy.key_element,
                   t_core_element_hierarchy.key_parent_element,
                   t_dm_tabledef.fk_element
                  FROM (common_dd.t_core_element_hierarchy
                    RIGHT JOIN common_dd.t_dm_tabledef ON ((t_dm_tabledef.fk_element = t_core_element_hierarchy.key_element)))
               UNION ALL
                SELECT t_core_element_hierarchy.key_element,
                   t_core_element_hierarchy.key_parent_element,
                   r_1.td
                  FROM common_dd.t_core_element_hierarchy,
                   r r_1
                 WHERE (r_1.k = t_core_element_hierarchy.key_parent_element)
               )
        SELECT r.k AS variable_key_element,
           v.dn AS variable_dn,
           v.short_name AS variable_short_name,
           r.td AS table_key_element,
           t.dn AS table_dn,
           t.short_name AS table_short_name,
           s.study_name
          FROM ((((r
            RIGHT JOIN common_dd.t_core_variable ON ((t_core_variable.key_variable = r.k)))
            LEFT JOIN common_dd.t_core_element_hierarchy v ON ((v.key_element = r.k)))
            LEFT JOIN common_dd.t_core_element_hierarchy t ON ((t.key_element = r.td)))
            LEFT JOIN common_dd.v_core_study s ON ((s.key_element = r.td)))
         WHERE (s.study_name IS NOT NULL);
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_dm_tablevar</code> table reference
     */
    public VDmTablevar(String alias) {
        this(DSL.name(alias), V_DM_TABLEVAR);
    }

    /**
     * Create an aliased <code>common_dd.v_dm_tablevar</code> table reference
     */
    public VDmTablevar(Name alias) {
        this(alias, V_DM_TABLEVAR);
    }

    /**
     * Create a <code>common_dd.v_dm_tablevar</code> table reference
     */
    public VDmTablevar() {
        this(DSL.name("v_dm_tablevar"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VDmTablevar as(String alias) {
        return new VDmTablevar(DSL.name(alias), this);
    }

    @Override
    public VDmTablevar as(Name alias) {
        return new VDmTablevar(alias, this);
    }

    @Override
    public VDmTablevar as(Table<?> alias) {
        return new VDmTablevar(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VDmTablevar rename(String name) {
        return new VDmTablevar(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDmTablevar rename(Name name) {
        return new VDmTablevar(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDmTablevar rename(Table<?> name) {
        return new VDmTablevar(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmTablevar where(Condition condition) {
        return new VDmTablevar(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmTablevar where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmTablevar where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmTablevar where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmTablevar where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmTablevar where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmTablevar where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmTablevar where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmTablevar whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmTablevar whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
