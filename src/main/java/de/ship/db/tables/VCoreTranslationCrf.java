/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.enums.EnumIsocode;
import de.ship.db.enums.EnumTransCategory;
import de.ship.db.tables.records.VCoreTranslationCrfRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreTranslationCrf extends TableImpl<VCoreTranslationCrfRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_core_translation_crf</code>
     */
    public static final VCoreTranslationCrf V_CORE_TRANSLATION_CRF = new VCoreTranslationCrf();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VCoreTranslationCrfRecord> getRecordType() {
        return VCoreTranslationCrfRecord.class;
    }

    /**
     * The column
     * <code>common_dd.v_core_translation_crf.pk_core_translationkey</code>.
     */
    public final TableField<VCoreTranslationCrfRecord, Integer> PK_CORE_TRANSLATIONKEY = createField(DSL.name("pk_core_translationkey"), SQLDataType.INTEGER, this, "");

    /**
     * The column
     * <code>common_dd.v_core_translation_crf.pk_core_translation</code>.
     */
    public final TableField<VCoreTranslationCrfRecord, Integer> PK_CORE_TRANSLATION = createField(DSL.name("pk_core_translation"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_translation_crf.fk_trans_key</code>.
     */
    public final TableField<VCoreTranslationCrfRecord, String> FK_TRANS_KEY = createField(DSL.name("fk_trans_key"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_translation_crf.isocode</code>.
     */
    public final TableField<VCoreTranslationCrfRecord, EnumIsocode> ISOCODE = createField(DSL.name("isocode"), SQLDataType.VARCHAR.asEnumDataType(EnumIsocode.class), this, "");

    /**
     * The column <code>common_dd.v_core_translation_crf.category</code>.
     */
    public final TableField<VCoreTranslationCrfRecord, EnumTransCategory> CATEGORY = createField(DSL.name("category"), SQLDataType.VARCHAR.asEnumDataType(EnumTransCategory.class), this, "");

    /**
     * The column <code>common_dd.v_core_translation_crf.value</code>.
     */
    public final TableField<VCoreTranslationCrfRecord, String> VALUE = createField(DSL.name("value"), SQLDataType.CLOB, this, "");

    private VCoreTranslationCrf(Name alias, Table<VCoreTranslationCrfRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VCoreTranslationCrf(Name alias, Table<VCoreTranslationCrfRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_core_translation_crf" as  SELECT pk_core_translationkey,
          pk_core_translation,
          fk_trans_key,
          isocode,
          category,
          value
         FROM common_dd.v_core_translation
        WHERE (category = 'crf'::common_dd.enum_trans_category);
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_core_translation_crf</code> table
     * reference
     */
    public VCoreTranslationCrf(String alias) {
        this(DSL.name(alias), V_CORE_TRANSLATION_CRF);
    }

    /**
     * Create an aliased <code>common_dd.v_core_translation_crf</code> table
     * reference
     */
    public VCoreTranslationCrf(Name alias) {
        this(alias, V_CORE_TRANSLATION_CRF);
    }

    /**
     * Create a <code>common_dd.v_core_translation_crf</code> table reference
     */
    public VCoreTranslationCrf() {
        this(DSL.name("v_core_translation_crf"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VCoreTranslationCrf as(String alias) {
        return new VCoreTranslationCrf(DSL.name(alias), this);
    }

    @Override
    public VCoreTranslationCrf as(Name alias) {
        return new VCoreTranslationCrf(alias, this);
    }

    @Override
    public VCoreTranslationCrf as(Table<?> alias) {
        return new VCoreTranslationCrf(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreTranslationCrf rename(String name) {
        return new VCoreTranslationCrf(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreTranslationCrf rename(Name name) {
        return new VCoreTranslationCrf(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreTranslationCrf rename(Table<?> name) {
        return new VCoreTranslationCrf(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreTranslationCrf where(Condition condition) {
        return new VCoreTranslationCrf(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreTranslationCrf where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreTranslationCrf where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreTranslationCrf where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreTranslationCrf where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreTranslationCrf where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreTranslationCrf where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreTranslationCrf where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreTranslationCrf whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreTranslationCrf whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
