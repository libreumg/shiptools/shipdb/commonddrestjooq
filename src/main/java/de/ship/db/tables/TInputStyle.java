/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.Keys;
import de.ship.db.tables.TInputLabel.TInputLabelPath;
import de.ship.db.tables.records.TInputStyleRecord;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.InverseForeignKey;
import org.jooq.Name;
import org.jooq.Path;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.Record;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TInputStyle extends TableImpl<TInputStyleRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.t_input_style</code>
     */
    public static final TInputStyle T_INPUT_STYLE = new TInputStyle();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TInputStyleRecord> getRecordType() {
        return TInputStyleRecord.class;
    }

    /**
     * The column <code>common_dd.t_input_style.pk_input_style</code>.
     */
    public final TableField<TInputStyleRecord, Integer> PK_INPUT_STYLE = createField(DSL.name("pk_input_style"), SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>common_dd.t_input_style.style_id</code>.
     */
    public final TableField<TInputStyleRecord, Integer> STYLE_ID = createField(DSL.name("style_id"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>common_dd.t_input_style.lastchange</code>.
     */
    public final TableField<TInputStyleRecord, LocalDateTime> LASTCHANGE = createField(DSL.name("lastchange"), SQLDataType.LOCALDATETIME(6).defaultValue(DSL.field(DSL.raw("now()"), SQLDataType.LOCALDATETIME)), this, "");

    private TInputStyle(Name alias, Table<TInputStyleRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private TInputStyle(Name alias, Table<TInputStyleRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table(), where);
    }

    /**
     * Create an aliased <code>common_dd.t_input_style</code> table reference
     */
    public TInputStyle(String alias) {
        this(DSL.name(alias), T_INPUT_STYLE);
    }

    /**
     * Create an aliased <code>common_dd.t_input_style</code> table reference
     */
    public TInputStyle(Name alias) {
        this(alias, T_INPUT_STYLE);
    }

    /**
     * Create a <code>common_dd.t_input_style</code> table reference
     */
    public TInputStyle() {
        this(DSL.name("t_input_style"), null);
    }

    public <O extends Record> TInputStyle(Table<O> path, ForeignKey<O, TInputStyleRecord> childPath, InverseForeignKey<O, TInputStyleRecord> parentPath) {
        super(path, childPath, parentPath, T_INPUT_STYLE);
    }

    /**
     * A subtype implementing {@link Path} for simplified path-based joins.
     */
    public static class TInputStylePath extends TInputStyle implements Path<TInputStyleRecord> {

        private static final long serialVersionUID = 1L;
        public <O extends Record> TInputStylePath(Table<O> path, ForeignKey<O, TInputStyleRecord> childPath, InverseForeignKey<O, TInputStyleRecord> parentPath) {
            super(path, childPath, parentPath);
        }
        private TInputStylePath(Name alias, Table<TInputStyleRecord> aliased) {
            super(alias, aliased);
        }

        @Override
        public TInputStylePath as(String alias) {
            return new TInputStylePath(DSL.name(alias), this);
        }

        @Override
        public TInputStylePath as(Name alias) {
            return new TInputStylePath(alias, this);
        }

        @Override
        public TInputStylePath as(Table<?> alias) {
            return new TInputStylePath(alias.getQualifiedName(), this);
        }
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public Identity<TInputStyleRecord, Integer> getIdentity() {
        return (Identity<TInputStyleRecord, Integer>) super.getIdentity();
    }

    @Override
    public UniqueKey<TInputStyleRecord> getPrimaryKey() {
        return Keys.PK_INPUT_STYLE;
    }

    @Override
    public List<UniqueKey<TInputStyleRecord>> getUniqueKeys() {
        return Arrays.asList(Keys.UK_INPUT_STYLE);
    }

    private transient TInputLabelPath _tInputLabel;

    /**
     * Get the implicit to-many join path to the
     * <code>common_dd.t_input_label</code> table
     */
    public TInputLabelPath tInputLabel() {
        if (_tInputLabel == null)
            _tInputLabel = new TInputLabelPath(this, null, Keys.T_INPUT_LABEL__FK_INPUT_LABEL_STYLE.getInverseKey());

        return _tInputLabel;
    }

    @Override
    public TInputStyle as(String alias) {
        return new TInputStyle(DSL.name(alias), this);
    }

    @Override
    public TInputStyle as(Name alias) {
        return new TInputStyle(alias, this);
    }

    @Override
    public TInputStyle as(Table<?> alias) {
        return new TInputStyle(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public TInputStyle rename(String name) {
        return new TInputStyle(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TInputStyle rename(Name name) {
        return new TInputStyle(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public TInputStyle rename(Table<?> name) {
        return new TInputStyle(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TInputStyle where(Condition condition) {
        return new TInputStyle(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TInputStyle where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TInputStyle where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TInputStyle where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TInputStyle where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TInputStyle where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TInputStyle where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TInputStyle where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TInputStyle whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TInputStyle whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
