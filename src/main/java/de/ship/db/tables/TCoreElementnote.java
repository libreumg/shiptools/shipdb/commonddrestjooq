/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.Keys;
import de.ship.db.enums.EnumElementnote;
import de.ship.db.tables.TCoreElementHierarchy.TCoreElementHierarchyPath;
import de.ship.db.tables.TCoreTranslationKey.TCoreTranslationKeyPath;
import de.ship.db.tables.records.TCoreElementnoteRecord;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.InverseForeignKey;
import org.jooq.Name;
import org.jooq.Path;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.Record;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreElementnote extends TableImpl<TCoreElementnoteRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.t_core_elementnote</code>
     */
    public static final TCoreElementnote T_CORE_ELEMENTNOTE = new TCoreElementnote();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TCoreElementnoteRecord> getRecordType() {
        return TCoreElementnoteRecord.class;
    }

    /**
     * The column <code>common_dd.t_core_elementnote.lastchange</code>.
     */
    public final TableField<TCoreElementnoteRecord, LocalDateTime> LASTCHANGE = createField(DSL.name("lastchange"), SQLDataType.LOCALDATETIME(6).defaultValue(DSL.field(DSL.raw("now()"), SQLDataType.LOCALDATETIME)), this, "");

    /**
     * The column <code>common_dd.t_core_elementnote.pk</code>. primary key
     */
    public final TableField<TCoreElementnoteRecord, Integer> PK = createField(DSL.name("pk"), SQLDataType.INTEGER.nullable(false).identity(true), this, "primary key");

    /**
     * The column <code>common_dd.t_core_elementnote.fk_element</code>.
     * reference to hierarchy element
     */
    public final TableField<TCoreElementnoteRecord, Integer> FK_ELEMENT = createField(DSL.name("fk_element"), SQLDataType.INTEGER.nullable(false), this, "reference to hierarchy element");

    /**
     * The column <code>common_dd.t_core_elementnote.note_type</code>. the type
     * of this node
     */
    public final TableField<TCoreElementnoteRecord, EnumElementnote> NOTE_TYPE = createField(DSL.name("note_type"), SQLDataType.VARCHAR.nullable(false).asEnumDataType(EnumElementnote.class), this, "the type of this node");

    /**
     * The column <code>common_dd.t_core_elementnote.fk_note_lang</code>.
     * reference to the translation of the note content
     */
    public final TableField<TCoreElementnoteRecord, String> FK_NOTE_LANG = createField(DSL.name("fk_note_lang"), SQLDataType.CLOB.nullable(false), this, "reference to the translation of the note content");

    private TCoreElementnote(Name alias, Table<TCoreElementnoteRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private TCoreElementnote(Name alias, Table<TCoreElementnoteRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table(), where);
    }

    /**
     * Create an aliased <code>common_dd.t_core_elementnote</code> table
     * reference
     */
    public TCoreElementnote(String alias) {
        this(DSL.name(alias), T_CORE_ELEMENTNOTE);
    }

    /**
     * Create an aliased <code>common_dd.t_core_elementnote</code> table
     * reference
     */
    public TCoreElementnote(Name alias) {
        this(alias, T_CORE_ELEMENTNOTE);
    }

    /**
     * Create a <code>common_dd.t_core_elementnote</code> table reference
     */
    public TCoreElementnote() {
        this(DSL.name("t_core_elementnote"), null);
    }

    public <O extends Record> TCoreElementnote(Table<O> path, ForeignKey<O, TCoreElementnoteRecord> childPath, InverseForeignKey<O, TCoreElementnoteRecord> parentPath) {
        super(path, childPath, parentPath, T_CORE_ELEMENTNOTE);
    }

    /**
     * A subtype implementing {@link Path} for simplified path-based joins.
     */
    public static class TCoreElementnotePath extends TCoreElementnote implements Path<TCoreElementnoteRecord> {

        private static final long serialVersionUID = 1L;
        public <O extends Record> TCoreElementnotePath(Table<O> path, ForeignKey<O, TCoreElementnoteRecord> childPath, InverseForeignKey<O, TCoreElementnoteRecord> parentPath) {
            super(path, childPath, parentPath);
        }
        private TCoreElementnotePath(Name alias, Table<TCoreElementnoteRecord> aliased) {
            super(alias, aliased);
        }

        @Override
        public TCoreElementnotePath as(String alias) {
            return new TCoreElementnotePath(DSL.name(alias), this);
        }

        @Override
        public TCoreElementnotePath as(Name alias) {
            return new TCoreElementnotePath(alias, this);
        }

        @Override
        public TCoreElementnotePath as(Table<?> alias) {
            return new TCoreElementnotePath(alias.getQualifiedName(), this);
        }
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public Identity<TCoreElementnoteRecord, Integer> getIdentity() {
        return (Identity<TCoreElementnoteRecord, Integer>) super.getIdentity();
    }

    @Override
    public UniqueKey<TCoreElementnoteRecord> getPrimaryKey() {
        return Keys.PK_CORE_ELEMENTNOTE;
    }

    @Override
    public List<UniqueKey<TCoreElementnoteRecord>> getUniqueKeys() {
        return Arrays.asList(Keys.UK_ELEMENTNOTE_ELEMENT_NOTETYPE);
    }

    @Override
    public List<ForeignKey<TCoreElementnoteRecord, ?>> getReferences() {
        return Arrays.asList(Keys.T_CORE_ELEMENTNOTE__FK_ELEMENT, Keys.T_CORE_ELEMENTNOTE__FK_NOTE_LANG);
    }

    private transient TCoreElementHierarchyPath _tCoreElementHierarchy;

    /**
     * Get the implicit join path to the
     * <code>common_dd.t_core_element_hierarchy</code> table.
     */
    public TCoreElementHierarchyPath tCoreElementHierarchy() {
        if (_tCoreElementHierarchy == null)
            _tCoreElementHierarchy = new TCoreElementHierarchyPath(this, Keys.T_CORE_ELEMENTNOTE__FK_ELEMENT, null);

        return _tCoreElementHierarchy;
    }

    private transient TCoreTranslationKeyPath _tCoreTranslationKey;

    /**
     * Get the implicit join path to the
     * <code>common_dd.t_core_translation_key</code> table.
     */
    public TCoreTranslationKeyPath tCoreTranslationKey() {
        if (_tCoreTranslationKey == null)
            _tCoreTranslationKey = new TCoreTranslationKeyPath(this, Keys.T_CORE_ELEMENTNOTE__FK_NOTE_LANG, null);

        return _tCoreTranslationKey;
    }

    @Override
    public TCoreElementnote as(String alias) {
        return new TCoreElementnote(DSL.name(alias), this);
    }

    @Override
    public TCoreElementnote as(Name alias) {
        return new TCoreElementnote(alias, this);
    }

    @Override
    public TCoreElementnote as(Table<?> alias) {
        return new TCoreElementnote(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreElementnote rename(String name) {
        return new TCoreElementnote(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreElementnote rename(Name name) {
        return new TCoreElementnote(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreElementnote rename(Table<?> name) {
        return new TCoreElementnote(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementnote where(Condition condition) {
        return new TCoreElementnote(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementnote where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementnote where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementnote where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementnote where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementnote where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementnote where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreElementnote where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementnote whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreElementnote whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
