/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VRestElementRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.JSONB;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VRestElement extends TableImpl<VRestElementRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_rest_element</code>
     */
    public static final VRestElement V_REST_ELEMENT = new VRestElement();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VRestElementRecord> getRecordType() {
        return VRestElementRecord.class;
    }

    /**
     * The column <code>common_dd.v_rest_element.unique_name</code>.
     */
    public final TableField<VRestElementRecord, String> UNIQUE_NAME = createField(DSL.name("unique_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.is_public</code>.
     */
    public final TableField<VRestElementRecord, Integer> IS_PUBLIC = createField(DSL.name("is_public"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_rest_element.order_nr</code>.
     */
    public final TableField<VRestElementRecord, Integer> ORDER_NR = createField(DSL.name("order_nr"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_rest_element.short_name</code>.
     */
    public final TableField<VRestElementRecord, String> SHORT_NAME = createField(DSL.name("short_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.stage</code>.
     */
    public final TableField<VRestElementRecord, Integer> STAGE = createField(DSL.name("stage"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_rest_element.parent_unique_name</code>.
     */
    public final TableField<VRestElementRecord, String> PARENT_UNIQUE_NAME = createField(DSL.name("parent_unique_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.type</code>.
     */
    public final TableField<VRestElementRecord, JSONB> TYPE = createField(DSL.name("type"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.translation</code>.
     */
    public final TableField<VRestElementRecord, JSONB> TRANSLATION = createField(DSL.name("translation"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.notes</code>.
     */
    public final TableField<VRestElementRecord, JSONB> NOTES = createField(DSL.name("notes"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.annotation</code>.
     */
    public final TableField<VRestElementRecord, JSONB> ANNOTATION = createField(DSL.name("annotation"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.v_rest_element.logic</code>.
     */
    public final TableField<VRestElementRecord, JSONB> LOGIC = createField(DSL.name("logic"), SQLDataType.JSONB, this, "");

    private VRestElement(Name alias, Table<VRestElementRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VRestElement(Name alias, Table<VRestElementRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
         create view "v_rest_element" as  WITH element_type(pk, name, description, translation) AS (
                 SELECT t.key_element_type,
                    t.name,
                    t.description,
                    jsonb_object_agg((l.isocode)::text, l.value) AS jsonb_object_agg
                   FROM (common_dd.t_core_elementtype t
                     LEFT JOIN common_dd.t_core_translation l ON ((l.fk_trans_key = t.elementtype_lang)))
                  GROUP BY t.key_element_type, t.name, t.description
                ), translation(pk, translation) AS (
                 SELECT t_core_translation.fk_trans_key,
                    jsonb_object_agg(t_core_translation.isocode, t_core_translation.value) AS jsonb_object_agg
                   FROM common_dd.t_core_translation
                  GROUP BY t_core_translation.fk_trans_key
                ), note(fk_element, notes) AS (
                 SELECT en.fk_element,
                    jsonb_object_agg(l.isocode, l.value) AS jsonb_object_agg
                   FROM (common_dd.t_core_elementnote en
                     LEFT JOIN common_dd.t_core_translation l ON ((l.fk_trans_key = en.fk_note_lang)))
                  WHERE (en.note_type = '1 - data quality for request form'::common_dd.enum_elementnote)
                  GROUP BY en.fk_element
                ), annotation(fk_element, source, annotation) AS (
                 SELECT t_core_annotation.fk_element,
                    t_core_annotation.source,
                    jsonb_object_agg(t_core_annotation.key, t_core_annotation.value) AS jsonb_object_agg
                   FROM common_dd.t_core_annotation
                  GROUP BY t_core_annotation.fk_element, t_core_annotation.source
                ), logics(logic, fk_element) AS (
                 WITH orgrps AS (
                         WITH andgrps AS (
                                 WITH logiccond AS (
                                         SELECT ll.fk_variable,
                                            lt.logictype_descr,
                                            land.fk_orgrp,
                                            land.andgrp,
                                            lcond.depth,
                                            lcond.depth2,
                                            COALESCE(var1.unique_name, var1.dn) AS left_side_var,
                                            lcond.logiccond_comparator AS comparator,
        CASE lcond.compare_variables
         WHEN 1 THEN COALESCE(var2.unique_name, var2.dn)
         ELSE (lcond.logiccond_value)::text
        END AS right_side_varorvalue,
                                            lcond.compare_variables
                                           FROM (((((common_dd.t_core_logic ll
                                             LEFT JOIN common_dd.t_core_logictype lt ON ((lt.logictype = ll.fk_logictype)))
                                             LEFT JOIN common_dd.t_core_logicandgrp land ON ((land.fk_orgrp = ll.fk_orgrp)))
                                             LEFT JOIN common_dd.t_core_logiccond lcond ON ((lcond.fk_andgrp = land.andgrp)))
                                             LEFT JOIN common_dd.t_core_element_hierarchy var1 ON ((var1.key_element = lcond.fk_variable)))
                                             LEFT JOIN common_dd.t_core_element_hierarchy var2 ON ((var2.key_element = lcond.fk_variable2)))
                                        )
                                 SELECT jsonb_agg(jsonb_build_object('comparator', logiccond.comparator, 'left_side', jsonb_build_object('unique_name', logiccond.left_side_var, 'multiobs_seek_depth', logiccond.depth), 'right_side',
                                        CASE logiccond.compare_variables
                                            WHEN 1 THEN jsonb_build_object('unique_name', logiccond.right_side_varorvalue, 'multiobs_seek_depth', logiccond.depth2)
                                            ELSE (logiccond.right_side_varorvalue)::jsonb
                                        END, 'compare_with_variable', logiccond.compare_variables)) AS andgrp,
                                    logiccond.fk_orgrp,
                                    logiccond.logictype_descr,
                                    logiccond.fk_variable
                                   FROM logiccond
                                  GROUP BY logiccond.andgrp, logiccond.fk_orgrp, logiccond.logictype_descr, logiccond.fk_variable
                                )
                         SELECT andgrps.logictype_descr,
                            jsonb_agg(andgrps.andgrp) AS orgrp,
                            andgrps.fk_variable
                           FROM andgrps
                          GROUP BY andgrps.fk_orgrp, andgrps.logictype_descr, andgrps.fk_variable
                        )
                 SELECT jsonb_object_agg(orgrps.logictype_descr, orgrps.orgrp) AS jsonb_object_agg,
                    orgrps.fk_variable
                   FROM orgrps
                  GROUP BY orgrps.fk_variable
                )
         SELECT COALESCE(h.unique_name, h.dn) AS unique_name,
            h.is_public,
            h.order_nr,
            h.short_name,
            h.stage,
            COALESCE(p.unique_name, p.dn) AS parent_unique_name,
            jsonb_insert(jsonb_build_object('name', element_type.name, 'description', element_type.description), '{translation}'::text[], element_type.translation) AS type,
            translation.translation,
            note.notes,
            jsonb_insert('{}'::jsonb, ((('{'::text || annotation.source) || '}'::text))::text[], annotation.annotation) AS annotation,
            logics.logic
           FROM ((((((common_dd.t_core_element_hierarchy h
             LEFT JOIN common_dd.t_core_element_hierarchy p ON ((p.key_element = h.key_parent_element)))
             LEFT JOIN element_type ON ((element_type.pk = h.key_element_type)))
             LEFT JOIN translation ON ((translation.pk = h.hier_lang)))
             LEFT JOIN note ON ((note.fk_element = h.key_element)))
             LEFT JOIN annotation ON ((annotation.fk_element = h.key_element)))
             LEFT JOIN logics ON ((logics.fk_element = h.key_element)));
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_rest_element</code> table reference
     */
    public VRestElement(String alias) {
        this(DSL.name(alias), V_REST_ELEMENT);
    }

    /**
     * Create an aliased <code>common_dd.v_rest_element</code> table reference
     */
    public VRestElement(Name alias) {
        this(alias, V_REST_ELEMENT);
    }

    /**
     * Create a <code>common_dd.v_rest_element</code> table reference
     */
    public VRestElement() {
        this(DSL.name("v_rest_element"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VRestElement as(String alias) {
        return new VRestElement(DSL.name(alias), this);
    }

    @Override
    public VRestElement as(Name alias) {
        return new VRestElement(alias, this);
    }

    @Override
    public VRestElement as(Table<?> alias) {
        return new VRestElement(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VRestElement rename(String name) {
        return new VRestElement(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VRestElement rename(Name name) {
        return new VRestElement(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VRestElement rename(Table<?> name) {
        return new VRestElement(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VRestElement where(Condition condition) {
        return new VRestElement(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VRestElement where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VRestElement where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VRestElement where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VRestElement where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VRestElement where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VRestElement where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VRestElement where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VRestElement whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VRestElement whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
