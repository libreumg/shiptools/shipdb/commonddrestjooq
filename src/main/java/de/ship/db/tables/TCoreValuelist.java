/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.Indexes;
import de.ship.db.Keys;
import de.ship.db.tables.TCoreValue.TCoreValuePath;
import de.ship.db.tables.TCoreValuetype.TCoreValuetypePath;
import de.ship.db.tables.TCoreVariable.TCoreVariablePath;
import de.ship.db.tables.records.TCoreValuelistRecord;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.InverseForeignKey;
import org.jooq.JSONB;
import org.jooq.Name;
import org.jooq.Path;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.Record;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreValuelist extends TableImpl<TCoreValuelistRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.t_core_valuelist</code>
     */
    public static final TCoreValuelist T_CORE_VALUELIST = new TCoreValuelist();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TCoreValuelistRecord> getRecordType() {
        return TCoreValuelistRecord.class;
    }

    /**
     * The column <code>common_dd.t_core_valuelist.lastchange</code>.
     */
    public final TableField<TCoreValuelistRecord, LocalDateTime> LASTCHANGE = createField(DSL.name("lastchange"), SQLDataType.LOCALDATETIME(6).defaultValue(DSL.field(DSL.raw("now()"), SQLDataType.LOCALDATETIME)), this, "");

    /**
     * The column <code>common_dd.t_core_valuelist.valuelist</code>.
     */
    public final TableField<TCoreValuelistRecord, Integer> VALUELIST = createField(DSL.name("valuelist"), SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>common_dd.t_core_valuelist.values</code>. list of values
     */
    public final TableField<TCoreValuelistRecord, JSONB> VALUES = createField(DSL.name("values"), SQLDataType.JSONB, this, "list of values");

    /**
     * The column <code>common_dd.t_core_valuelist.fk_valuetype</code>. type of
     * values structure
     */
    public final TableField<TCoreValuelistRecord, Integer> FK_VALUETYPE = createField(DSL.name("fk_valuetype"), SQLDataType.INTEGER.nullable(false), this, "type of values structure");

    /**
     * The column <code>common_dd.t_core_valuelist.globallist</code>. is global
     * list
     */
    public final TableField<TCoreValuelistRecord, Boolean> GLOBALLIST = createField(DSL.name("globallist"), SQLDataType.BOOLEAN.nullable(false).defaultValue(DSL.field(DSL.raw("false"), SQLDataType.BOOLEAN)), this, "is global list");

    /**
     * The column <code>common_dd.t_core_valuelist.changeable</code>. allow
     * changes
     */
    public final TableField<TCoreValuelistRecord, Boolean> CHANGEABLE = createField(DSL.name("changeable"), SQLDataType.BOOLEAN.nullable(false).defaultValue(DSL.field(DSL.raw("false"), SQLDataType.BOOLEAN)), this, "allow changes");

    /**
     * The column <code>common_dd.t_core_valuelist.origin</code>.
     */
    public final TableField<TCoreValuelistRecord, JSONB> ORIGIN = createField(DSL.name("origin"), SQLDataType.JSONB, this, "");

    /**
     * The column <code>common_dd.t_core_valuelist.name</code>.
     */
    public final TableField<TCoreValuelistRecord, String> NAME = createField(DSL.name("name"), SQLDataType.CLOB, this, "");

    private TCoreValuelist(Name alias, Table<TCoreValuelistRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private TCoreValuelist(Name alias, Table<TCoreValuelistRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table(), where);
    }

    /**
     * Create an aliased <code>common_dd.t_core_valuelist</code> table reference
     */
    public TCoreValuelist(String alias) {
        this(DSL.name(alias), T_CORE_VALUELIST);
    }

    /**
     * Create an aliased <code>common_dd.t_core_valuelist</code> table reference
     */
    public TCoreValuelist(Name alias) {
        this(alias, T_CORE_VALUELIST);
    }

    /**
     * Create a <code>common_dd.t_core_valuelist</code> table reference
     */
    public TCoreValuelist() {
        this(DSL.name("t_core_valuelist"), null);
    }

    public <O extends Record> TCoreValuelist(Table<O> path, ForeignKey<O, TCoreValuelistRecord> childPath, InverseForeignKey<O, TCoreValuelistRecord> parentPath) {
        super(path, childPath, parentPath, T_CORE_VALUELIST);
    }

    /**
     * A subtype implementing {@link Path} for simplified path-based joins.
     */
    public static class TCoreValuelistPath extends TCoreValuelist implements Path<TCoreValuelistRecord> {

        private static final long serialVersionUID = 1L;
        public <O extends Record> TCoreValuelistPath(Table<O> path, ForeignKey<O, TCoreValuelistRecord> childPath, InverseForeignKey<O, TCoreValuelistRecord> parentPath) {
            super(path, childPath, parentPath);
        }
        private TCoreValuelistPath(Name alias, Table<TCoreValuelistRecord> aliased) {
            super(alias, aliased);
        }

        @Override
        public TCoreValuelistPath as(String alias) {
            return new TCoreValuelistPath(DSL.name(alias), this);
        }

        @Override
        public TCoreValuelistPath as(Name alias) {
            return new TCoreValuelistPath(alias, this);
        }

        @Override
        public TCoreValuelistPath as(Table<?> alias) {
            return new TCoreValuelistPath(alias.getQualifiedName(), this);
        }
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.asList(Indexes.IDX_CORE_VALUELIST_FK_VALUETYPE);
    }

    @Override
    public Identity<TCoreValuelistRecord, Integer> getIdentity() {
        return (Identity<TCoreValuelistRecord, Integer>) super.getIdentity();
    }

    @Override
    public UniqueKey<TCoreValuelistRecord> getPrimaryKey() {
        return Keys.PK_CORE_VALUELIST;
    }

    @Override
    public List<UniqueKey<TCoreValuelistRecord>> getUniqueKeys() {
        return Arrays.asList(Keys.T_CORE_VALUELIST_NAME_KEY);
    }

    @Override
    public List<ForeignKey<TCoreValuelistRecord, ?>> getReferences() {
        return Arrays.asList(Keys.T_CORE_VALUELIST__FK_CORE_VALUELIST_VALUETYPE);
    }

    private transient TCoreValuetypePath _tCoreValuetype;

    /**
     * Get the implicit join path to the <code>common_dd.t_core_valuetype</code>
     * table.
     */
    public TCoreValuetypePath tCoreValuetype() {
        if (_tCoreValuetype == null)
            _tCoreValuetype = new TCoreValuetypePath(this, Keys.T_CORE_VALUELIST__FK_CORE_VALUELIST_VALUETYPE, null);

        return _tCoreValuetype;
    }

    private transient TCoreValuePath _tCoreValue;

    /**
     * Get the implicit to-many join path to the
     * <code>common_dd.t_core_value</code> table
     */
    public TCoreValuePath tCoreValue() {
        if (_tCoreValue == null)
            _tCoreValue = new TCoreValuePath(this, null, Keys.T_CORE_VALUE__FK_VALUE_VALUELIST.getInverseKey());

        return _tCoreValue;
    }

    private transient TCoreVariablePath _tCoreVariable;

    /**
     * Get the implicit to-many join path to the
     * <code>common_dd.t_core_variable</code> table
     */
    public TCoreVariablePath tCoreVariable() {
        if (_tCoreVariable == null)
            _tCoreVariable = new TCoreVariablePath(this, null, Keys.T_CORE_VARIABLE__FK_VAR_VALUELIST.getInverseKey());

        return _tCoreVariable;
    }

    @Override
    public TCoreValuelist as(String alias) {
        return new TCoreValuelist(DSL.name(alias), this);
    }

    @Override
    public TCoreValuelist as(Name alias) {
        return new TCoreValuelist(alias, this);
    }

    @Override
    public TCoreValuelist as(Table<?> alias) {
        return new TCoreValuelist(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreValuelist rename(String name) {
        return new TCoreValuelist(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreValuelist rename(Name name) {
        return new TCoreValuelist(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public TCoreValuelist rename(Table<?> name) {
        return new TCoreValuelist(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreValuelist where(Condition condition) {
        return new TCoreValuelist(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreValuelist where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreValuelist where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreValuelist where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreValuelist where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreValuelist where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreValuelist where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public TCoreValuelist where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreValuelist whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public TCoreValuelist whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
