/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VCoreUniqueNamePrefixRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreUniqueNamePrefix extends TableImpl<VCoreUniqueNamePrefixRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of
     * <code>common_dd.v_core_unique_name_prefix</code>
     */
    public static final VCoreUniqueNamePrefix V_CORE_UNIQUE_NAME_PREFIX = new VCoreUniqueNamePrefix();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VCoreUniqueNamePrefixRecord> getRecordType() {
        return VCoreUniqueNamePrefixRecord.class;
    }

    /**
     * The column <code>common_dd.v_core_unique_name_prefix.prefix</code>.
     */
    public final TableField<VCoreUniqueNamePrefixRecord, String> PREFIX = createField(DSL.name("prefix"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_unique_name_prefix.key_element</code>.
     */
    public final TableField<VCoreUniqueNamePrefixRecord, Integer> KEY_ELEMENT = createField(DSL.name("key_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_unique_name_prefix.short</code>.
     */
    public final TableField<VCoreUniqueNamePrefixRecord, String> SHORT = createField(DSL.name("short"), SQLDataType.CLOB, this, "");

    private VCoreUniqueNamePrefix(Name alias, Table<VCoreUniqueNamePrefixRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VCoreUniqueNamePrefix(Name alias, Table<VCoreUniqueNamePrefixRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_core_unique_name_prefix" as  WITH RECURSIVE t(k, p, short, prefix) AS (
                SELECT t_core_element_hierarchy.key_element,
                   t_core_element_hierarchy.key_parent_element,
                   t_core_element_hierarchy.short_name,
                   sg.unique_name_prefix
                  FROM (common_dd.t_core_element_hierarchy
                    LEFT JOIN common_dd.t_core_studygroup sg ON ((sg.fk_element = t_core_element_hierarchy.key_element)))
                 WHERE (t_core_element_hierarchy.key_parent_element IS NULL)
               UNION ALL
                SELECT t_core_element_hierarchy.key_element,
                   t_core_element_hierarchy.key_parent_element,
                   t_core_element_hierarchy.short_name,
                   COALESCE(s.unique_name_prefix, t_1.prefix) AS "coalesce"
                  FROM ((common_dd.t_core_element_hierarchy
                    LEFT JOIN common_dd.t_core_study s ON ((s.fk_element = t_core_element_hierarchy.key_element)))
                    JOIN t t_1 ON ((t_1.k = t_core_element_hierarchy.key_parent_element)))
               )
        SELECT prefix,
           k AS key_element,
           short
          FROM t;
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_core_unique_name_prefix</code> table
     * reference
     */
    public VCoreUniqueNamePrefix(String alias) {
        this(DSL.name(alias), V_CORE_UNIQUE_NAME_PREFIX);
    }

    /**
     * Create an aliased <code>common_dd.v_core_unique_name_prefix</code> table
     * reference
     */
    public VCoreUniqueNamePrefix(Name alias) {
        this(alias, V_CORE_UNIQUE_NAME_PREFIX);
    }

    /**
     * Create a <code>common_dd.v_core_unique_name_prefix</code> table reference
     */
    public VCoreUniqueNamePrefix() {
        this(DSL.name("v_core_unique_name_prefix"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VCoreUniqueNamePrefix as(String alias) {
        return new VCoreUniqueNamePrefix(DSL.name(alias), this);
    }

    @Override
    public VCoreUniqueNamePrefix as(Name alias) {
        return new VCoreUniqueNamePrefix(alias, this);
    }

    @Override
    public VCoreUniqueNamePrefix as(Table<?> alias) {
        return new VCoreUniqueNamePrefix(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreUniqueNamePrefix rename(String name) {
        return new VCoreUniqueNamePrefix(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreUniqueNamePrefix rename(Name name) {
        return new VCoreUniqueNamePrefix(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreUniqueNamePrefix rename(Table<?> name) {
        return new VCoreUniqueNamePrefix(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreUniqueNamePrefix where(Condition condition) {
        return new VCoreUniqueNamePrefix(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreUniqueNamePrefix where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreUniqueNamePrefix where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreUniqueNamePrefix where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreUniqueNamePrefix where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreUniqueNamePrefix where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreUniqueNamePrefix where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreUniqueNamePrefix where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreUniqueNamePrefix whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreUniqueNamePrefix whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
