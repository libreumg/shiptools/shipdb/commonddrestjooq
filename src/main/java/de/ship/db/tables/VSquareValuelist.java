/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VSquareValuelistRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VSquareValuelist extends TableImpl<VSquareValuelistRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_square_valuelist</code>
     */
    public static final VSquareValuelist V_SQUARE_VALUELIST = new VSquareValuelist();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VSquareValuelistRecord> getRecordType() {
        return VSquareValuelistRecord.class;
    }

    /**
     * The column <code>common_dd.v_square_valuelist.key_variable</code>.
     */
    public final TableField<VSquareValuelistRecord, Integer> KEY_VARIABLE = createField(DSL.name("key_variable"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_square_valuelist.valuelist</code>.
     */
    public final TableField<VSquareValuelistRecord, String> VALUELIST = createField(DSL.name("valuelist"), SQLDataType.CLOB, this, "");

    private VSquareValuelist(Name alias, Table<VSquareValuelistRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VSquareValuelist(Name alias, Table<VSquareValuelistRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_square_valuelist" as  SELECT v.key_variable,
          string_agg(((k.varvalue || '='::text) || l.value), ','::text) AS valuelist
         FROM (((common_dd.t_core_variable v
           LEFT JOIN common_dd.t_core_value k ON ((k.fk_value_valuelist = v.fk_var_valuelist)))
           LEFT JOIN common_dd.t_core_valueparam p ON ((p.fk_valueparam_value = k.pk_core_value)))
           LEFT JOIN common_dd.t_core_translation l ON (((l.fk_trans_key = p.param_key) AND (l.isocode = 'de_DE'::common_dd.enum_isocode) AND (l.category = 'crf'::common_dd.enum_trans_category))))
        GROUP BY v.key_variable;
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_square_valuelist</code> table
     * reference
     */
    public VSquareValuelist(String alias) {
        this(DSL.name(alias), V_SQUARE_VALUELIST);
    }

    /**
     * Create an aliased <code>common_dd.v_square_valuelist</code> table
     * reference
     */
    public VSquareValuelist(Name alias) {
        this(alias, V_SQUARE_VALUELIST);
    }

    /**
     * Create a <code>common_dd.v_square_valuelist</code> table reference
     */
    public VSquareValuelist() {
        this(DSL.name("v_square_valuelist"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VSquareValuelist as(String alias) {
        return new VSquareValuelist(DSL.name(alias), this);
    }

    @Override
    public VSquareValuelist as(Name alias) {
        return new VSquareValuelist(alias, this);
    }

    @Override
    public VSquareValuelist as(Table<?> alias) {
        return new VSquareValuelist(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VSquareValuelist rename(String name) {
        return new VSquareValuelist(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VSquareValuelist rename(Name name) {
        return new VSquareValuelist(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VSquareValuelist rename(Table<?> name) {
        return new VSquareValuelist(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VSquareValuelist where(Condition condition) {
        return new VSquareValuelist(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VSquareValuelist where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VSquareValuelist where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VSquareValuelist where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VSquareValuelist where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VSquareValuelist where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VSquareValuelist where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VSquareValuelist where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VSquareValuelist whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VSquareValuelist whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
