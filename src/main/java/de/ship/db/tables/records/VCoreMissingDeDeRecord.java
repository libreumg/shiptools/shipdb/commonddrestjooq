/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreMissingDeDe;

import java.time.LocalDateTime;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreMissingDeDeRecord extends TableRecordImpl<VCoreMissingDeDeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.fk_missinglist</code>.
     */
    public void setFkMissinglist(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.fk_missinglist</code>.
     */
    public Integer getFkMissinglist() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.missing_value</code>.
     */
    public void setMissingValue(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.missing_value</code>.
     */
    public Integer getMissingValue() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.value_lang</code>.
     */
    public void setValueLang(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.value_lang</code>.
     */
    public String getValueLang() {
        return (String) get(2);
    }

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.de_de</code>.
     */
    public void setDeDe(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.de_de</code>.
     */
    public String getDeDe() {
        return (String) get(3);
    }

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.num_missingcode</code>.
     */
    public void setNumMissingcode(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.num_missingcode</code>.
     */
    public Integer getNumMissingcode() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.sas_missingcode</code>.
     */
    public void setSasMissingcode(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.sas_missingcode</code>.
     */
    public String getSasMissingcode() {
        return (String) get(5);
    }

    /**
     * Setter for <code>common_dd.v_core_missing_de_de.date_missingcode</code>.
     */
    public void setDateMissingcode(LocalDateTime value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missing_de_de.date_missingcode</code>.
     */
    public LocalDateTime getDateMissingcode() {
        return (LocalDateTime) get(6);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreMissingDeDeRecord
     */
    public VCoreMissingDeDeRecord() {
        super(VCoreMissingDeDe.V_CORE_MISSING_DE_DE);
    }

    /**
     * Create a detached, initialised VCoreMissingDeDeRecord
     */
    public VCoreMissingDeDeRecord(Integer fkMissinglist, Integer missingValue, String valueLang, String deDe, Integer numMissingcode, String sasMissingcode, LocalDateTime dateMissingcode) {
        super(VCoreMissingDeDe.V_CORE_MISSING_DE_DE);

        setFkMissinglist(fkMissinglist);
        setMissingValue(missingValue);
        setValueLang(valueLang);
        setDeDe(deDe);
        setNumMissingcode(numMissingcode);
        setSasMissingcode(sasMissingcode);
        setDateMissingcode(dateMissingcode);
        resetChangedOnNotNull();
    }
}
