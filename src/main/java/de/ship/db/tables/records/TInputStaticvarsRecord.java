/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TInputStaticvars;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TInputStaticvarsRecord extends UpdatableRecordImpl<TInputStaticvarsRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_input_staticvars.pk_input_staticvars</code>.
     */
    public void setPkInputStaticvars(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_input_staticvars.pk_input_staticvars</code>.
     */
    public Integer getPkInputStaticvars() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.t_input_staticvars.fk_variable</code>.
     */
    public void setFkVariable(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_input_staticvars.fk_variable</code>.
     */
    public Integer getFkVariable() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_input_staticvars.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_input_staticvars.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TInputStaticvarsRecord
     */
    public TInputStaticvarsRecord() {
        super(TInputStaticvars.T_INPUT_STATICVARS);
    }

    /**
     * Create a detached, initialised TInputStaticvarsRecord
     */
    public TInputStaticvarsRecord(Integer pkInputStaticvars, Integer fkVariable, LocalDateTime lastchange) {
        super(TInputStaticvars.T_INPUT_STATICVARS);

        setPkInputStaticvars(pkInputStaticvars);
        setFkVariable(fkVariable);
        setLastchange(lastchange);
        resetChangedOnNotNull();
    }
}
