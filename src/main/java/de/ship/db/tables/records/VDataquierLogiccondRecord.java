/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VDataquierLogiccond;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDataquierLogiccondRecord extends TableRecordImpl<VDataquierLogiccondRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_dataquier_logiccond.fk_element</code>.
     */
    public void setFkElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_logiccond.fk_element</code>.
     */
    public Integer getFkElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_dataquier_logiccond.logictype_descr</code>.
     */
    public void setLogictypeDescr(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_logiccond.logictype_descr</code>.
     */
    public String getLogictypeDescr() {
        return (String) get(1);
    }

    /**
     * Setter for <code>common_dd.v_dataquier_logiccond.fk_orgrp</code>.
     */
    public void setFkOrgrp(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_logiccond.fk_orgrp</code>.
     */
    public Integer getFkOrgrp() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.v_dataquier_logiccond.cond</code>.
     */
    public void setCond(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_logiccond.cond</code>.
     */
    public String getCond() {
        return (String) get(3);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VDataquierLogiccondRecord
     */
    public VDataquierLogiccondRecord() {
        super(VDataquierLogiccond.V_DATAQUIER_LOGICCOND);
    }

    /**
     * Create a detached, initialised VDataquierLogiccondRecord
     */
    public VDataquierLogiccondRecord(Integer fkElement, String logictypeDescr, Integer fkOrgrp, String cond) {
        super(VDataquierLogiccond.V_DATAQUIER_LOGICCOND);

        setFkElement(fkElement);
        setLogictypeDescr(logictypeDescr);
        setFkOrgrp(fkOrgrp);
        setCond(cond);
        resetChangedOnNotNull();
    }
}
