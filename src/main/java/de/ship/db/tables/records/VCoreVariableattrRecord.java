/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreVariableattr;

import org.jooq.JSONB;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreVariableattrRecord extends TableRecordImpl<VCoreVariableattrRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_variableattr.unique_name</code>.
     */
    public void setUniqueName(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_variableattr.unique_name</code>.
     */
    public String getUniqueName() {
        return (String) get(0);
    }

    /**
     * Setter for <code>common_dd.v_core_variableattr.value</code>.
     */
    public void setValue(JSONB value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_core_variableattr.value</code>.
     */
    public JSONB getValue() {
        return (JSONB) get(1);
    }

    /**
     * Setter for <code>common_dd.v_core_variableattr.key</code>.
     */
    public void setKey(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_core_variableattr.key</code>.
     */
    public String getKey() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreVariableattrRecord
     */
    public VCoreVariableattrRecord() {
        super(VCoreVariableattr.V_CORE_VARIABLEATTR);
    }

    /**
     * Create a detached, initialised VCoreVariableattrRecord
     */
    public VCoreVariableattrRecord(String uniqueName, JSONB value, String key) {
        super(VCoreVariableattr.V_CORE_VARIABLEATTR);

        setUniqueName(uniqueName);
        setValue(value);
        setKey(key);
        resetChangedOnNotNull();
    }
}
