/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VNovariable;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VNovariableRecord extends TableRecordImpl<VNovariableRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_novariable.key_element</code>.
     */
    public void setKeyElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.key_element</code>.
     */
    public Integer getKeyElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_novariable.key_element_type</code>.
     */
    public void setKeyElementType(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.key_element_type</code>.
     */
    public Integer getKeyElementType() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.v_novariable.key_parent_element</code>.
     */
    public void setKeyParentElement(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.key_parent_element</code>.
     */
    public Integer getKeyParentElement() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.v_novariable.dn</code>.
     */
    public void setDn(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.dn</code>.
     */
    public String getDn() {
        return (String) get(3);
    }

    /**
     * Setter for <code>common_dd.v_novariable.order_nr</code>.
     */
    public void setOrderNr(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.order_nr</code>.
     */
    public Integer getOrderNr() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>common_dd.v_novariable.short_name</code>.
     */
    public void setShortName(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.short_name</code>.
     */
    public String getShortName() {
        return (String) get(5);
    }

    /**
     * Setter for <code>common_dd.v_novariable.is_public</code>.
     */
    public void setIsPublic(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.is_public</code>.
     */
    public Integer getIsPublic() {
        return (Integer) get(6);
    }

    /**
     * Setter for <code>common_dd.v_novariable.hier_lang</code>.
     */
    public void setHierLang(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.v_novariable.hier_lang</code>.
     */
    public String getHierLang() {
        return (String) get(7);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VNovariableRecord
     */
    public VNovariableRecord() {
        super(VNovariable.V_NOVARIABLE);
    }

    /**
     * Create a detached, initialised VNovariableRecord
     */
    public VNovariableRecord(Integer keyElement, Integer keyElementType, Integer keyParentElement, String dn, Integer orderNr, String shortName, Integer isPublic, String hierLang) {
        super(VNovariable.V_NOVARIABLE);

        setKeyElement(keyElement);
        setKeyElementType(keyElementType);
        setKeyParentElement(keyParentElement);
        setDn(dn);
        setOrderNr(orderNr);
        setShortName(shortName);
        setIsPublic(isPublic);
        setHierLang(hierLang);
        resetChangedOnNotNull();
    }
}
