/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.enums.EnumIsocode;
import de.ship.db.enums.EnumTransCategory;
import de.ship.db.tables.VCoreElementLabel;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreElementLabelRecord extends TableRecordImpl<VCoreElementLabelRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_element_label.unique_name</code>.
     */
    public void setUniqueName(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_label.unique_name</code>.
     */
    public String getUniqueName() {
        return (String) get(0);
    }

    /**
     * Setter for <code>common_dd.v_core_element_label.fk_element</code>.
     */
    public void setFkElement(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_label.fk_element</code>.
     */
    public Integer getFkElement() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.v_core_element_label.value</code>.
     */
    public void setValue(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_label.value</code>.
     */
    public String getValue() {
        return (String) get(2);
    }

    /**
     * Setter for <code>common_dd.v_core_element_label.isocode</code>.
     */
    public void setIsocode(EnumIsocode value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_label.isocode</code>.
     */
    public EnumIsocode getIsocode() {
        return (EnumIsocode) get(3);
    }

    /**
     * Setter for <code>common_dd.v_core_element_label.category</code>.
     */
    public void setCategory(EnumTransCategory value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_label.category</code>.
     */
    public EnumTransCategory getCategory() {
        return (EnumTransCategory) get(4);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreElementLabelRecord
     */
    public VCoreElementLabelRecord() {
        super(VCoreElementLabel.V_CORE_ELEMENT_LABEL);
    }

    /**
     * Create a detached, initialised VCoreElementLabelRecord
     */
    public VCoreElementLabelRecord(String uniqueName, Integer fkElement, String value, EnumIsocode isocode, EnumTransCategory category) {
        super(VCoreElementLabel.V_CORE_ELEMENT_LABEL);

        setUniqueName(uniqueName);
        setFkElement(fkElement);
        setValue(value);
        setIsocode(isocode);
        setCategory(category);
        resetChangedOnNotNull();
    }
}
