/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TCoreStage;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreStageRecord extends UpdatableRecordImpl<TCoreStageRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_core_stage.pk_core_stage</code>.
     */
    public void setPkCoreStage(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.pk_core_stage</code>.
     */
    public Integer getPkCoreStage() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.t_core_stage.stage</code>. unique id of stage
     */
    public void setStage(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.stage</code>. unique id of stage
     */
    public Integer getStage() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_core_stage.stage_dev</code>. stage is
     * developement
     */
    public void setStageDev(Boolean value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.stage_dev</code>. stage is
     * developement
     */
    public Boolean getStageDev() {
        return (Boolean) get(2);
    }

    /**
     * Setter for <code>common_dd.t_core_stage.stage_test</code>. stage is test
     */
    public void setStageTest(Boolean value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.stage_test</code>. stage is test
     */
    public Boolean getStageTest() {
        return (Boolean) get(3);
    }

    /**
     * Setter for <code>common_dd.t_core_stage.stage_prod</code>. stage is
     * productive
     */
    public void setStageProd(Boolean value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.stage_prod</code>. stage is
     * productive
     */
    public Boolean getStageProd() {
        return (Boolean) get(4);
    }

    /**
     * Setter for <code>common_dd.t_core_stage.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(5);
    }

    /**
     * Setter for <code>common_dd.t_core_stage.stage_lang</code>.
     */
    public void setStageLang(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.t_core_stage.stage_lang</code>.
     */
    public String getStageLang() {
        return (String) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TCoreStageRecord
     */
    public TCoreStageRecord() {
        super(TCoreStage.T_CORE_STAGE);
    }

    /**
     * Create a detached, initialised TCoreStageRecord
     */
    public TCoreStageRecord(Integer pkCoreStage, Integer stage, Boolean stageDev, Boolean stageTest, Boolean stageProd, LocalDateTime lastchange, String stageLang) {
        super(TCoreStage.T_CORE_STAGE);

        setPkCoreStage(pkCoreStage);
        setStage(stage);
        setStageDev(stageDev);
        setStageTest(stageTest);
        setStageProd(stageProd);
        setLastchange(lastchange);
        setStageLang(stageLang);
        resetChangedOnNotNull();
    }
}
