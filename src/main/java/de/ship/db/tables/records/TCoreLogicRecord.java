/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.enums.EnumLogicvalidreason;
import de.ship.db.tables.TCoreLogic;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreLogicRecord extends UpdatableRecordImpl<TCoreLogicRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_core_logic.pk_core_logic</code>.
     */
    public void setPkCoreLogic(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.pk_core_logic</code>.
     */
    public Integer getPkCoreLogic() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.fk_logictype</code>.
     */
    public void setFkLogictype(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.fk_logictype</code>.
     */
    public Integer getFkLogictype() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.fk_orgrp</code>.
     */
    public void setFkOrgrp(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.fk_orgrp</code>.
     */
    public Integer getFkOrgrp() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.fk_variable</code>.
     */
    public void setFkVariable(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.fk_variable</code>.
     */
    public Integer getFkVariable() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(4);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.valid_from</code>.
     */
    public void setValidFrom(LocalDateTime value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.valid_from</code>.
     */
    public LocalDateTime getValidFrom() {
        return (LocalDateTime) get(5);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.valid_until</code>.
     */
    public void setValidUntil(LocalDateTime value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.valid_until</code>.
     */
    public LocalDateTime getValidUntil() {
        return (LocalDateTime) get(6);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.valid_comment</code>.
     */
    public void setValidComment(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.valid_comment</code>.
     */
    public String getValidComment() {
        return (String) get(7);
    }

    /**
     * Setter for <code>common_dd.t_core_logic.valid_reason</code>.
     */
    public void setValidReason(EnumLogicvalidreason value) {
        set(8, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logic.valid_reason</code>.
     */
    public EnumLogicvalidreason getValidReason() {
        return (EnumLogicvalidreason) get(8);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TCoreLogicRecord
     */
    public TCoreLogicRecord() {
        super(TCoreLogic.T_CORE_LOGIC);
    }

    /**
     * Create a detached, initialised TCoreLogicRecord
     */
    public TCoreLogicRecord(Integer pkCoreLogic, Integer fkLogictype, Integer fkOrgrp, Integer fkVariable, LocalDateTime lastchange, LocalDateTime validFrom, LocalDateTime validUntil, String validComment, EnumLogicvalidreason validReason) {
        super(TCoreLogic.T_CORE_LOGIC);

        setPkCoreLogic(pkCoreLogic);
        setFkLogictype(fkLogictype);
        setFkOrgrp(fkOrgrp);
        setFkVariable(fkVariable);
        setLastchange(lastchange);
        setValidFrom(validFrom);
        setValidUntil(validUntil);
        setValidComment(validComment);
        setValidReason(validReason);
        resetChangedOnNotNull();
    }
}
