/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TCoreLifetime;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreLifetimeRecord extends UpdatableRecordImpl<TCoreLifetimeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_core_lifetime.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_core_lifetime.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(0);
    }

    /**
     * Setter for <code>common_dd.t_core_lifetime.fk_element</code>. foreign key
     * to key_element
     */
    public void setFkElement(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_core_lifetime.fk_element</code>. foreign key
     * to key_element
     */
    public Integer getFkElement() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_core_lifetime.valid_from</code>. valid from;
     * if null, date is unknown
     */
    public void setValidFrom(LocalDateTime value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_core_lifetime.valid_from</code>. valid from;
     * if null, date is unknown
     */
    public LocalDateTime getValidFrom() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>common_dd.t_core_lifetime.valid_until</code>. valid
     * until; if null, date is unknown (until now)
     */
    public void setValidUntil(LocalDateTime value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_core_lifetime.valid_until</code>. valid
     * until; if null, date is unknown (until now)
     */
    public LocalDateTime getValidUntil() {
        return (LocalDateTime) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TCoreLifetimeRecord
     */
    public TCoreLifetimeRecord() {
        super(TCoreLifetime.T_CORE_LIFETIME);
    }

    /**
     * Create a detached, initialised TCoreLifetimeRecord
     */
    public TCoreLifetimeRecord(LocalDateTime lastchange, Integer fkElement, LocalDateTime validFrom, LocalDateTime validUntil) {
        super(TCoreLifetime.T_CORE_LIFETIME);

        setLastchange(lastchange);
        setFkElement(fkElement);
        setValidFrom(validFrom);
        setValidUntil(validUntil);
        resetChangedOnNotNull();
    }
}
