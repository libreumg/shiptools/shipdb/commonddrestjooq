/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.enums.EnumTemplatestyle;
import de.ship.db.tables.TInputExport;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TInputExportRecord extends UpdatableRecordImpl<TInputExportRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_input_export.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(0);
    }

    /**
     * Setter for <code>common_dd.t_input_export.pk</code>.
     */
    public void setPk(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_input_export.fk_element</code>. foreign key
     * to key_element of referenced form
     */
    public void setFkElement(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.fk_element</code>. foreign key
     * to key_element of referenced form
     */
    public Integer getFkElement() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.t_input_export.template_code</code>. code of
     * template
     */
    public void setTemplateCode(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.template_code</code>. code of
     * template
     */
    public String getTemplateCode() {
        return (String) get(3);
    }

    /**
     * Setter for <code>common_dd.t_input_export.template_style</code>. style of
     * template (plain, latex)
     */
    public void setTemplateStyle(EnumTemplatestyle value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.template_style</code>. style of
     * template (plain, latex)
     */
    public EnumTemplatestyle getTemplateStyle() {
        return (EnumTemplatestyle) get(4);
    }

    /**
     * Setter for <code>common_dd.t_input_export.template_name</code>. name of
     * read template; can contain template variables
     */
    public void setTemplateName(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.template_name</code>. name of
     * read template; can contain template variables
     */
    public String getTemplateName() {
        return (String) get(5);
    }

    /**
     * Setter for <code>common_dd.t_input_export.template_class</code>.
     * deprecated; use template_style instead; class of template (0 - plain, 1 -
     * latex)
     */
    public void setTemplateClass(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.t_input_export.template_class</code>.
     * deprecated; use template_style instead; class of template (0 - plain, 1 -
     * latex)
     */
    public Integer getTemplateClass() {
        return (Integer) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TInputExportRecord
     */
    public TInputExportRecord() {
        super(TInputExport.T_INPUT_EXPORT);
    }

    /**
     * Create a detached, initialised TInputExportRecord
     */
    public TInputExportRecord(LocalDateTime lastchange, Integer pk, Integer fkElement, String templateCode, EnumTemplatestyle templateStyle, String templateName, Integer templateClass) {
        super(TInputExport.T_INPUT_EXPORT);

        setLastchange(lastchange);
        setPk(pk);
        setFkElement(fkElement);
        setTemplateCode(templateCode);
        setTemplateStyle(templateStyle);
        setTemplateName(templateName);
        setTemplateClass(templateClass);
        resetChangedOnNotNull();
    }
}
