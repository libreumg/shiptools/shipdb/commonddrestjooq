/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TInputHelp;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TInputHelpRecord extends UpdatableRecordImpl<TInputHelpRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_input_help.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_input_help.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(0);
    }

    /**
     * Setter for <code>common_dd.t_input_help.inputhelp</code>. id of input
     * help
     */
    public void setInputhelp(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_input_help.inputhelp</code>. id of input
     * help
     */
    public Integer getInputhelp() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_input_help.inputhelp_descr</code>. language
     * key of input help
     */
    public void setInputhelpDescr(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_input_help.inputhelp_descr</code>. language
     * key of input help
     */
    public String getInputhelpDescr() {
        return (String) get(2);
    }

    /**
     * Setter for <code>common_dd.t_input_help.needed_params</code>. number of
     * helpparams needed for this inputhelper
     */
    public void setNeededParams(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_input_help.needed_params</code>. number of
     * helpparams needed for this inputhelper
     */
    public Integer getNeededParams() {
        return (Integer) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TInputHelpRecord
     */
    public TInputHelpRecord() {
        super(TInputHelp.T_INPUT_HELP);
    }

    /**
     * Create a detached, initialised TInputHelpRecord
     */
    public TInputHelpRecord(LocalDateTime lastchange, Integer inputhelp, String inputhelpDescr, Integer neededParams) {
        super(TInputHelp.T_INPUT_HELP);

        setLastchange(lastchange);
        setInputhelp(inputhelp);
        setInputhelpDescr(inputhelpDescr);
        setNeededParams(neededParams);
        resetChangedOnNotNull();
    }
}
