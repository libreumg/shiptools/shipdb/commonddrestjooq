/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VVersion;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VVersionRecord extends TableRecordImpl<VVersionRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_version.db_version</code>.
     */
    public void setDbVersion(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_version.db_version</code>.
     */
    public Integer getDbVersion() {
        return (Integer) get(0);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VVersionRecord
     */
    public VVersionRecord() {
        super(VVersion.V_VERSION);
    }

    /**
     * Create a detached, initialised VVersionRecord
     */
    public VVersionRecord(Integer dbVersion) {
        super(VVersion.V_VERSION);

        setDbVersion(dbVersion);
        resetChangedOnNotNull();
    }
}
