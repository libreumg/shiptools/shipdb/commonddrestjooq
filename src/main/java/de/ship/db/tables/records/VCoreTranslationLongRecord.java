/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.enums.EnumIsocode;
import de.ship.db.enums.EnumTransCategory;
import de.ship.db.tables.VCoreTranslationLong;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreTranslationLongRecord extends TableRecordImpl<VCoreTranslationLongRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for
     * <code>common_dd.v_core_translation_long.pk_core_translationkey</code>.
     */
    public void setPkCoreTranslationkey(Integer value) {
        set(0, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_translation_long.pk_core_translationkey</code>.
     */
    public Integer getPkCoreTranslationkey() {
        return (Integer) get(0);
    }

    /**
     * Setter for
     * <code>common_dd.v_core_translation_long.pk_core_translation</code>.
     */
    public void setPkCoreTranslation(Integer value) {
        set(1, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_translation_long.pk_core_translation</code>.
     */
    public Integer getPkCoreTranslation() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.v_core_translation_long.fk_trans_key</code>.
     */
    public void setFkTransKey(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_core_translation_long.fk_trans_key</code>.
     */
    public String getFkTransKey() {
        return (String) get(2);
    }

    /**
     * Setter for <code>common_dd.v_core_translation_long.isocode</code>.
     */
    public void setIsocode(EnumIsocode value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_core_translation_long.isocode</code>.
     */
    public EnumIsocode getIsocode() {
        return (EnumIsocode) get(3);
    }

    /**
     * Setter for <code>common_dd.v_core_translation_long.category</code>.
     */
    public void setCategory(EnumTransCategory value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_core_translation_long.category</code>.
     */
    public EnumTransCategory getCategory() {
        return (EnumTransCategory) get(4);
    }

    /**
     * Setter for <code>common_dd.v_core_translation_long.value</code>.
     */
    public void setValue(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.v_core_translation_long.value</code>.
     */
    public String getValue() {
        return (String) get(5);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreTranslationLongRecord
     */
    public VCoreTranslationLongRecord() {
        super(VCoreTranslationLong.V_CORE_TRANSLATION_LONG);
    }

    /**
     * Create a detached, initialised VCoreTranslationLongRecord
     */
    public VCoreTranslationLongRecord(Integer pkCoreTranslationkey, Integer pkCoreTranslation, String fkTransKey, EnumIsocode isocode, EnumTransCategory category, String value) {
        super(VCoreTranslationLong.V_CORE_TRANSLATION_LONG);

        setPkCoreTranslationkey(pkCoreTranslationkey);
        setPkCoreTranslation(pkCoreTranslation);
        setFkTransKey(fkTransKey);
        setIsocode(isocode);
        setCategory(category);
        setValue(value);
        resetChangedOnNotNull();
    }
}
