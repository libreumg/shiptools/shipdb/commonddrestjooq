/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VDmMailing;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDmMailingRecord extends TableRecordImpl<VDmMailingRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_dm_mailing.fk_dm_person</code>.
     */
    public void setFkDmPerson(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.fk_dm_person</code>.
     */
    public Integer getFkDmPerson() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.tree</code>.
     */
    public void setTree(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.tree</code>.
     */
    public String getTree() {
        return (String) get(1);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.dn</code>.
     */
    public void setDn(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.dn</code>.
     */
    public String getDn() {
        return (String) get(2);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.key_element</code>.
     */
    public void setKeyElement(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.key_element</code>.
     */
    public Integer getKeyElement() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.key_element_type</code>.
     */
    public void setKeyElementType(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.key_element_type</code>.
     */
    public Integer getKeyElementType() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.varname</code>.
     */
    public void setVarname(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.varname</code>.
     */
    public String getVarname() {
        return (String) get(5);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.var_long_ger</code>.
     */
    public void setVarLongGer(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.var_long_ger</code>.
     */
    public String getVarLongGer() {
        return (String) get(6);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.fk_usnr</code>.
     */
    public void setFkUsnr(Integer value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.fk_usnr</code>.
     */
    public Integer getFkUsnr() {
        return (Integer) get(7);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.user_mail</code>.
     */
    public void setUserMail(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.user_mail</code>.
     */
    public String getUserMail() {
        return (String) get(8);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.recip_id</code>.
     */
    public void setRecipId(Integer value) {
        set(9, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.recip_id</code>.
     */
    public Integer getRecipId() {
        return (Integer) get(9);
    }

    /**
     * Setter for <code>common_dd.v_dm_mailing.value</code>.
     */
    public void setValue(String value) {
        set(10, value);
    }

    /**
     * Getter for <code>common_dd.v_dm_mailing.value</code>.
     */
    public String getValue() {
        return (String) get(10);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VDmMailingRecord
     */
    public VDmMailingRecord() {
        super(VDmMailing.V_DM_MAILING);
    }

    /**
     * Create a detached, initialised VDmMailingRecord
     */
    public VDmMailingRecord(Integer fkDmPerson, String tree, String dn, Integer keyElement, Integer keyElementType, String varname, String varLongGer, Integer fkUsnr, String userMail, Integer recipId, String value) {
        super(VDmMailing.V_DM_MAILING);

        setFkDmPerson(fkDmPerson);
        setTree(tree);
        setDn(dn);
        setKeyElement(keyElement);
        setKeyElementType(keyElementType);
        setVarname(varname);
        setVarLongGer(varLongGer);
        setFkUsnr(fkUsnr);
        setUserMail(userMail);
        setRecipId(recipId);
        setValue(value);
        resetChangedOnNotNull();
    }
}
