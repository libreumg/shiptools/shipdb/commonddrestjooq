/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VSquareValuelist;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VSquareValuelistRecord extends TableRecordImpl<VSquareValuelistRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_square_valuelist.key_variable</code>.
     */
    public void setKeyVariable(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_square_valuelist.key_variable</code>.
     */
    public Integer getKeyVariable() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_square_valuelist.valuelist</code>.
     */
    public void setValuelist(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_square_valuelist.valuelist</code>.
     */
    public String getValuelist() {
        return (String) get(1);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VSquareValuelistRecord
     */
    public VSquareValuelistRecord() {
        super(VSquareValuelist.V_SQUARE_VALUELIST);
    }

    /**
     * Create a detached, initialised VSquareValuelistRecord
     */
    public VSquareValuelistRecord(Integer keyVariable, String valuelist) {
        super(VSquareValuelist.V_SQUARE_VALUELIST);

        setKeyVariable(keyVariable);
        setValuelist(valuelist);
        resetChangedOnNotNull();
    }
}
