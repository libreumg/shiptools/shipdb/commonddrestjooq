/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VDataquierNotes;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDataquierNotesRecord extends TableRecordImpl<VDataquierNotesRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_dataquier_notes.fk_element</code>.
     */
    public void setFkElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_notes.fk_element</code>.
     */
    public Integer getFkElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_dataquier_notes.note_de</code>.
     */
    public void setNoteDe(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_notes.note_de</code>.
     */
    public String getNoteDe() {
        return (String) get(1);
    }

    /**
     * Setter for <code>common_dd.v_dataquier_notes.note_en</code>.
     */
    public void setNoteEn(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_dataquier_notes.note_en</code>.
     */
    public String getNoteEn() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VDataquierNotesRecord
     */
    public VDataquierNotesRecord() {
        super(VDataquierNotes.V_DATAQUIER_NOTES);
    }

    /**
     * Create a detached, initialised VDataquierNotesRecord
     */
    public VDataquierNotesRecord(Integer fkElement, String noteDe, String noteEn) {
        super(VDataquierNotes.V_DATAQUIER_NOTES);

        setFkElement(fkElement);
        setNoteDe(noteDe);
        setNoteEn(noteEn);
        resetChangedOnNotNull();
    }
}
