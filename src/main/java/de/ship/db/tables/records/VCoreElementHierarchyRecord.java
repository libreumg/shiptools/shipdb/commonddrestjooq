/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreElementHierarchy;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreElementHierarchyRecord extends TableRecordImpl<VCoreElementHierarchyRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.key_element</code>.
     */
    public void setKeyElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.key_element</code>.
     */
    public Integer getKeyElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for
     * <code>common_dd.v_core_element_hierarchy.key_element_type</code>.
     */
    public void setKeyElementType(Integer value) {
        set(1, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_element_hierarchy.key_element_type</code>.
     */
    public Integer getKeyElementType() {
        return (Integer) get(1);
    }

    /**
     * Setter for
     * <code>common_dd.v_core_element_hierarchy.key_parent_element</code>.
     */
    public void setKeyParentElement(Integer value) {
        set(2, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_element_hierarchy.key_parent_element</code>.
     */
    public Integer getKeyParentElement() {
        return (Integer) get(2);
    }

    /**
     * Setter for
     * <code>common_dd.v_core_element_hierarchy.child_elements</code>.
     */
    public void setChildElements(Long value) {
        set(3, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_element_hierarchy.child_elements</code>.
     */
    public Long getChildElements() {
        return (Long) get(3);
    }

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.child_vars</code>.
     */
    public void setChildVars(Long value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.child_vars</code>.
     */
    public Long getChildVars() {
        return (Long) get(4);
    }

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.dn</code>.
     */
    public void setDn(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.dn</code>.
     */
    public String getDn() {
        return (String) get(5);
    }

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.order_nr</code>.
     */
    public void setOrderNr(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.order_nr</code>.
     */
    public Integer getOrderNr() {
        return (Integer) get(6);
    }

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.short_name</code>.
     */
    public void setShortName(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.short_name</code>.
     */
    public String getShortName() {
        return (String) get(7);
    }

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.is_public</code>.
     */
    public void setIsPublic(Integer value) {
        set(8, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.is_public</code>.
     */
    public Integer getIsPublic() {
        return (Integer) get(8);
    }

    /**
     * Setter for <code>common_dd.v_core_element_hierarchy.hier_lang</code>.
     */
    public void setHierLang(String value) {
        set(9, value);
    }

    /**
     * Getter for <code>common_dd.v_core_element_hierarchy.hier_lang</code>.
     */
    public String getHierLang() {
        return (String) get(9);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreElementHierarchyRecord
     */
    public VCoreElementHierarchyRecord() {
        super(VCoreElementHierarchy.V_CORE_ELEMENT_HIERARCHY);
    }

    /**
     * Create a detached, initialised VCoreElementHierarchyRecord
     */
    public VCoreElementHierarchyRecord(Integer keyElement, Integer keyElementType, Integer keyParentElement, Long childElements, Long childVars, String dn, Integer orderNr, String shortName, Integer isPublic, String hierLang) {
        super(VCoreElementHierarchy.V_CORE_ELEMENT_HIERARCHY);

        setKeyElement(keyElement);
        setKeyElementType(keyElementType);
        setKeyParentElement(keyParentElement);
        setChildElements(childElements);
        setChildVars(childVars);
        setDn(dn);
        setOrderNr(orderNr);
        setShortName(shortName);
        setIsPublic(isPublic);
        setHierLang(hierLang);
        resetChangedOnNotNull();
    }
}
