/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreValuelist;

import org.jooq.JSONB;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreValuelistRecord extends TableRecordImpl<VCoreValuelistRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_valuelist.valuelist</code>.
     */
    public void setValuelist(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_valuelist.valuelist</code>.
     */
    public Integer getValuelist() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_core_valuelist.key</code>.
     */
    public void setKey(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_core_valuelist.key</code>.
     */
    public String getKey() {
        return (String) get(1);
    }

    /**
     * Setter for <code>common_dd.v_core_valuelist.value</code>.
     */
    public void setValue(JSONB value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_core_valuelist.value</code>.
     */
    public JSONB getValue() {
        return (JSONB) get(2);
    }

    /**
     * Setter for <code>common_dd.v_core_valuelist.globallist</code>.
     */
    public void setGloballist(Boolean value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_core_valuelist.globallist</code>.
     */
    public Boolean getGloballist() {
        return (Boolean) get(3);
    }

    /**
     * Setter for <code>common_dd.v_core_valuelist.changeable</code>.
     */
    public void setChangeable(Boolean value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_core_valuelist.changeable</code>.
     */
    public Boolean getChangeable() {
        return (Boolean) get(4);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreValuelistRecord
     */
    public VCoreValuelistRecord() {
        super(VCoreValuelist.V_CORE_VALUELIST);
    }

    /**
     * Create a detached, initialised VCoreValuelistRecord
     */
    public VCoreValuelistRecord(Integer valuelist, String key, JSONB value, Boolean globallist, Boolean changeable) {
        super(VCoreValuelist.V_CORE_VALUELIST);

        setValuelist(valuelist);
        setKey(key);
        setValue(value);
        setGloballist(globallist);
        setChangeable(changeable);
        resetChangedOnNotNull();
    }
}
