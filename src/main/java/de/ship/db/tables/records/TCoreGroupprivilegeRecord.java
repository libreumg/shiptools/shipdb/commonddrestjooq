/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.enums.EnumAccesslevel;
import de.ship.db.tables.TCoreGroupprivilege;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreGroupprivilegeRecord extends UpdatableRecordImpl<TCoreGroupprivilegeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(0);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.pk</code>.
     */
    public void setPk(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.fk_privilege</code>.
     */
    public void setFkPrivilege(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.fk_privilege</code>.
     */
    public Integer getFkPrivilege() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.fk_group</code>.
     */
    public void setFkGroup(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.fk_group</code>.
     */
    public Integer getFkGroup() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.shippie_acl</code>.
     */
    public void setShippieAcl(EnumAccesslevel value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.shippie_acl</code>.
     */
    public EnumAccesslevel getShippieAcl() {
        return (EnumAccesslevel) get(4);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.shipdesigner_acl</code>.
     */
    public void setShipdesignerAcl(EnumAccesslevel value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.shipdesigner_acl</code>.
     */
    public EnumAccesslevel getShipdesignerAcl() {
        return (EnumAccesslevel) get(5);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.square_acl</code>.
     */
    public void setSquareAcl(EnumAccesslevel value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.square_acl</code>.
     */
    public EnumAccesslevel getSquareAcl() {
        return (EnumAccesslevel) get(6);
    }

    /**
     * Setter for <code>common_dd.t_core_groupprivilege.sam_acl</code>.
     */
    public void setSamAcl(EnumAccesslevel value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.t_core_groupprivilege.sam_acl</code>.
     */
    public EnumAccesslevel getSamAcl() {
        return (EnumAccesslevel) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TCoreGroupprivilegeRecord
     */
    public TCoreGroupprivilegeRecord() {
        super(TCoreGroupprivilege.T_CORE_GROUPPRIVILEGE);
    }

    /**
     * Create a detached, initialised TCoreGroupprivilegeRecord
     */
    public TCoreGroupprivilegeRecord(LocalDateTime lastchange, Integer pk, Integer fkPrivilege, Integer fkGroup, EnumAccesslevel shippieAcl, EnumAccesslevel shipdesignerAcl, EnumAccesslevel squareAcl, EnumAccesslevel samAcl) {
        super(TCoreGroupprivilege.T_CORE_GROUPPRIVILEGE);

        setLastchange(lastchange);
        setPk(pk);
        setFkPrivilege(fkPrivilege);
        setFkGroup(fkGroup);
        setShippieAcl(shippieAcl);
        setShipdesignerAcl(shipdesignerAcl);
        setSquareAcl(squareAcl);
        setSamAcl(samAcl);
        resetChangedOnNotNull();
    }
}
