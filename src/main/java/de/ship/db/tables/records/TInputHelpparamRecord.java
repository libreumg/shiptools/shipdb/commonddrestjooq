/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TInputHelpparam;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TInputHelpparamRecord extends UpdatableRecordImpl<TInputHelpparamRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_input_helpparam.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_input_helpparam.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(0);
    }

    /**
     * Setter for <code>common_dd.t_input_helpparam.pk</code>.
     */
    public void setPk(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_input_helpparam.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_input_helpparam.fk_study</code>. foreign key
     * to key_element of study
     */
    public void setFkStudy(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_input_helpparam.fk_study</code>. foreign key
     * to key_element of study
     */
    public Integer getFkStudy() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.t_input_helpparam.fk_variable</code>. foreign
     * key to key_element of parameter variable
     */
    public void setFkVariable(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_input_helpparam.fk_variable</code>. foreign
     * key to key_element of parameter variable
     */
    public Integer getFkVariable() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>common_dd.t_input_helpparam.fk_inputhelp</code>. foreign
     * key to inputhelper this parameter belongs to
     */
    public void setFkInputhelp(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_input_helpparam.fk_inputhelp</code>. foreign
     * key to inputhelper this parameter belongs to
     */
    public Integer getFkInputhelp() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>common_dd.t_input_helpparam.order_nr</code>. order of
     * the parameter
     */
    public void setOrderNr(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_input_helpparam.order_nr</code>. order of
     * the parameter
     */
    public Integer getOrderNr() {
        return (Integer) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TInputHelpparamRecord
     */
    public TInputHelpparamRecord() {
        super(TInputHelpparam.T_INPUT_HELPPARAM);
    }

    /**
     * Create a detached, initialised TInputHelpparamRecord
     */
    public TInputHelpparamRecord(LocalDateTime lastchange, Integer pk, Integer fkStudy, Integer fkVariable, Integer fkInputhelp, Integer orderNr) {
        super(TInputHelpparam.T_INPUT_HELPPARAM);

        setLastchange(lastchange);
        setPk(pk);
        setFkStudy(fkStudy);
        setFkVariable(fkVariable);
        setFkInputhelp(fkInputhelp);
        setOrderNr(orderNr);
        resetChangedOnNotNull();
    }
}
