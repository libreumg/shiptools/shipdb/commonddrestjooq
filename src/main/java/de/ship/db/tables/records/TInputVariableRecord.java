/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TInputVariable;

import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TInputVariableRecord extends UpdatableRecordImpl<TInputVariableRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_input_variable.pk_input_variable</code>.
     */
    public void setPkInputVariable(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.pk_input_variable</code>.
     */
    public Integer getPkInputVariable() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.fk_variable</code>.
     */
    public void setFkVariable(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.fk_variable</code>.
     */
    public Integer getFkVariable() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.var_order</code>. input order
     * inside of this variable group
     */
    public void setVarOrder(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.var_order</code>. input order
     * inside of this variable group
     */
    public Integer getVarOrder() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.var_ecrf</code>. visibility
     * on data input; 1 = yes (default), 0 = no (decativated)
     */
    public void setVarEcrf(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.var_ecrf</code>. visibility
     * on data input; 1 = yes (default), 0 = no (decativated)
     */
    public Integer getVarEcrf() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.var_formposx</code>. x
     * position of this element
     */
    public void setVarFormposx(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.var_formposx</code>. x
     * position of this element
     */
    public Integer getVarFormposx() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.var_formposy</code>. y
     * position of this element
     */
    public void setVarFormposy(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.var_formposy</code>. y
     * position of this element
     */
    public Integer getVarFormposy() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.fk_var_inputtype</code>.
     */
    public void setFkVarInputtype(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.fk_var_inputtype</code>.
     */
    public Integer getFkVarInputtype() {
        return (Integer) get(6);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.default_value</code>. default
     * value; must be number if used
     */
    public void setDefaultValue(Integer value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.default_value</code>. default
     * value; must be number if used
     */
    public Integer getDefaultValue() {
        return (Integer) get(7);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.fk_var_inputhelp</code>.
     */
    public void setFkVarInputhelp(Integer value) {
        set(8, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.fk_var_inputhelp</code>.
     */
    public Integer getFkVarInputhelp() {
        return (Integer) get(8);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.label_position</code>.
     * position of label; 0 = none, 1 = left, 2 = top, 3 = right, 4 = bottom
     */
    public void setLabelPosition(Integer value) {
        set(9, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.label_position</code>.
     * position of label; 0 = none, 1 = left, 2 = top, 3 = right, 4 = bottom
     */
    public Integer getLabelPosition() {
        return (Integer) get(9);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(10, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(10);
    }

    /**
     * Setter for <code>common_dd.t_input_variable.required</code>.
     */
    public void setRequired(Boolean value) {
        set(11, value);
    }

    /**
     * Getter for <code>common_dd.t_input_variable.required</code>.
     */
    public Boolean getRequired() {
        return (Boolean) get(11);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TInputVariableRecord
     */
    public TInputVariableRecord() {
        super(TInputVariable.T_INPUT_VARIABLE);
    }

    /**
     * Create a detached, initialised TInputVariableRecord
     */
    public TInputVariableRecord(Integer pkInputVariable, Integer fkVariable, Integer varOrder, Integer varEcrf, Integer varFormposx, Integer varFormposy, Integer fkVarInputtype, Integer defaultValue, Integer fkVarInputhelp, Integer labelPosition, LocalDateTime lastchange, Boolean required) {
        super(TInputVariable.T_INPUT_VARIABLE);

        setPkInputVariable(pkInputVariable);
        setFkVariable(fkVariable);
        setVarOrder(varOrder);
        setVarEcrf(varEcrf);
        setVarFormposx(varFormposx);
        setVarFormposy(varFormposy);
        setFkVarInputtype(fkVarInputtype);
        setDefaultValue(defaultValue);
        setFkVarInputhelp(fkVarInputhelp);
        setLabelPosition(labelPosition);
        setLastchange(lastchange);
        setRequired(required);
        resetChangedOnNotNull();
    }
}
