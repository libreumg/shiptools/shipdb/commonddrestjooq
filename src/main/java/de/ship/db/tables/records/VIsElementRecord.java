/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VIsElement;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VIsElementRecord extends TableRecordImpl<VIsElementRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_is_element.key_element</code>.
     */
    public void setKeyElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_is_element.key_element</code>.
     */
    public Integer getKeyElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_is_element.dn</code>.
     */
    public void setDn(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_is_element.dn</code>.
     */
    public String getDn() {
        return (String) get(1);
    }

    /**
     * Setter for <code>common_dd.v_is_element.element_type</code>.
     */
    public void setElementType(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_is_element.element_type</code>.
     */
    public String getElementType() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VIsElementRecord
     */
    public VIsElementRecord() {
        super(VIsElement.V_IS_ELEMENT);
    }

    /**
     * Create a detached, initialised VIsElementRecord
     */
    public VIsElementRecord(Integer keyElement, String dn, String elementType) {
        super(VIsElement.V_IS_ELEMENT);

        setKeyElement(keyElement);
        setDn(dn);
        setElementType(elementType);
        resetChangedOnNotNull();
    }
}
