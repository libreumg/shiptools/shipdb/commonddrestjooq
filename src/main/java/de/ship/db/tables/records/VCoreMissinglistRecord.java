/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreMissinglist;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreMissinglistRecord extends TableRecordImpl<VCoreMissinglistRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_missinglist.key_variable</code>.
     */
    public void setKeyVariable(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missinglist.key_variable</code>.
     */
    public Integer getKeyVariable() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_core_missinglist.num_missings</code>.
     */
    public void setNumMissings(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_core_missinglist.num_missings</code>.
     */
    public String getNumMissings() {
        return (String) get(1);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreMissinglistRecord
     */
    public VCoreMissinglistRecord() {
        super(VCoreMissinglist.V_CORE_MISSINGLIST);
    }

    /**
     * Create a detached, initialised VCoreMissinglistRecord
     */
    public VCoreMissinglistRecord(Integer keyVariable, String numMissings) {
        super(VCoreMissinglist.V_CORE_MISSINGLIST);

        setKeyVariable(keyVariable);
        setNumMissings(numMissings);
        resetChangedOnNotNull();
    }
}
