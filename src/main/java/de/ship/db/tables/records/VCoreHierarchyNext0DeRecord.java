/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreHierarchyNext0De;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreHierarchyNext0DeRecord extends TableRecordImpl<VCoreHierarchyNext0DeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_hierarchy_next0_de.key_element</code>.
     */
    public void setKeyElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_hierarchy_next0_de.key_element</code>.
     */
    public Integer getKeyElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for
     * <code>common_dd.v_core_hierarchy_next0_de.key_element_type</code>.
     */
    public void setKeyElementType(Integer value) {
        set(1, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_hierarchy_next0_de.key_element_type</code>.
     */
    public Integer getKeyElementType() {
        return (Integer) get(1);
    }

    /**
     * Setter for
     * <code>common_dd.v_core_hierarchy_next0_de.key_parent_element</code>.
     */
    public void setKeyParentElement(Integer value) {
        set(2, value);
    }

    /**
     * Getter for
     * <code>common_dd.v_core_hierarchy_next0_de.key_parent_element</code>.
     */
    public Integer getKeyParentElement() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.v_core_hierarchy_next0_de.dn</code>.
     */
    public void setDn(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.v_core_hierarchy_next0_de.dn</code>.
     */
    public String getDn() {
        return (String) get(3);
    }

    /**
     * Setter for <code>common_dd.v_core_hierarchy_next0_de.short_name</code>.
     */
    public void setShortName(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.v_core_hierarchy_next0_de.short_name</code>.
     */
    public String getShortName() {
        return (String) get(4);
    }

    /**
     * Setter for <code>common_dd.v_core_hierarchy_next0_de.hier_lang</code>.
     */
    public void setHierLang(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.v_core_hierarchy_next0_de.hier_lang</code>.
     */
    public String getHierLang() {
        return (String) get(5);
    }

    /**
     * Setter for <code>common_dd.v_core_hierarchy_next0_de.order_nr</code>.
     */
    public void setOrderNr(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.v_core_hierarchy_next0_de.order_nr</code>.
     */
    public Integer getOrderNr() {
        return (Integer) get(6);
    }

    /**
     * Setter for <code>common_dd.v_core_hierarchy_next0_de.lang_value</code>.
     */
    public void setLangValue(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.v_core_hierarchy_next0_de.lang_value</code>.
     */
    public String getLangValue() {
        return (String) get(7);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreHierarchyNext0DeRecord
     */
    public VCoreHierarchyNext0DeRecord() {
        super(VCoreHierarchyNext0De.V_CORE_HIERARCHY_NEXT0_DE);
    }

    /**
     * Create a detached, initialised VCoreHierarchyNext0DeRecord
     */
    public VCoreHierarchyNext0DeRecord(Integer keyElement, Integer keyElementType, Integer keyParentElement, String dn, String shortName, String hierLang, Integer orderNr, String langValue) {
        super(VCoreHierarchyNext0De.V_CORE_HIERARCHY_NEXT0_DE);

        setKeyElement(keyElement);
        setKeyElementType(keyElementType);
        setKeyParentElement(keyParentElement);
        setDn(dn);
        setShortName(shortName);
        setHierLang(hierLang);
        setOrderNr(orderNr);
        setLangValue(langValue);
        resetChangedOnNotNull();
    }
}
