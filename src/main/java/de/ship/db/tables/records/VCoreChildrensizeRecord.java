/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.VCoreChildrensize;

import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreChildrensizeRecord extends TableRecordImpl<VCoreChildrensizeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.v_core_childrensize.key_element</code>.
     */
    public void setKeyElement(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.v_core_childrensize.key_element</code>.
     */
    public Integer getKeyElement() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.v_core_childrensize.unique_name</code>.
     */
    public void setUniqueName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.v_core_childrensize.unique_name</code>.
     */
    public String getUniqueName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>common_dd.v_core_childrensize.number_of_children</code>.
     */
    public void setNumberOfChildren(Long value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.v_core_childrensize.number_of_children</code>.
     */
    public Long getNumberOfChildren() {
        return (Long) get(2);
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VCoreChildrensizeRecord
     */
    public VCoreChildrensizeRecord() {
        super(VCoreChildrensize.V_CORE_CHILDRENSIZE);
    }

    /**
     * Create a detached, initialised VCoreChildrensizeRecord
     */
    public VCoreChildrensizeRecord(Integer keyElement, String uniqueName, Long numberOfChildren) {
        super(VCoreChildrensize.V_CORE_CHILDRENSIZE);

        setKeyElement(keyElement);
        setUniqueName(uniqueName);
        setNumberOfChildren(numberOfChildren);
        resetChangedOnNotNull();
    }
}
