/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables.records;


import de.ship.db.tables.TCoreLogiccond;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TCoreLogiccondRecord extends UpdatableRecordImpl<TCoreLogiccondRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>common_dd.t_core_logiccond.pk_core_logiccond</code>.
     */
    public void setPkCoreLogiccond(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.pk_core_logiccond</code>.
     */
    public Integer getPkCoreLogiccond() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.fk_andgrp</code>.
     */
    public void setFkAndgrp(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.fk_andgrp</code>.
     */
    public Integer getFkAndgrp() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.fk_variable</code>.
     */
    public void setFkVariable(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.fk_variable</code>.
     */
    public Integer getFkVariable() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.depth</code>.
     */
    public void setDepth(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.depth</code>.
     */
    public Integer getDepth() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.logiccond_value</code>. value
     * of logical condition (if is not variable)
     */
    public void setLogiccondValue(BigDecimal value) {
        set(4, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.logiccond_value</code>. value
     * of logical condition (if is not variable)
     */
    public BigDecimal getLogiccondValue() {
        return (BigDecimal) get(4);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.fk_variable2</code>. value of
     * logical condition (if is variable)
     */
    public void setFkVariable2(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.fk_variable2</code>. value of
     * logical condition (if is variable)
     */
    public Integer getFkVariable2() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.depth2</code>.
     */
    public void setDepth2(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.depth2</code>.
     */
    public Integer getDepth2() {
        return (Integer) get(6);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.logiccond_comparator</code>.
     * comparator; may be =, !=, &gt;, &lt;, &gt;=, &lt;=, is null, is not null
     */
    public void setLogiccondComparator(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.logiccond_comparator</code>.
     * comparator; may be =, !=, &gt;, &lt;, &gt;=, &lt;=, is null, is not null
     */
    public String getLogiccondComparator() {
        return (String) get(7);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.compare_variables</code>. use
     * fk_variable2 (= 1) or logiccond_value (= 0)
     */
    public void setCompareVariables(Integer value) {
        set(8, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.compare_variables</code>. use
     * fk_variable2 (= 1) or logiccond_value (= 0)
     */
    public Integer getCompareVariables() {
        return (Integer) get(8);
    }

    /**
     * Setter for <code>common_dd.t_core_logiccond.lastchange</code>.
     */
    public void setLastchange(LocalDateTime value) {
        set(9, value);
    }

    /**
     * Getter for <code>common_dd.t_core_logiccond.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(9);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TCoreLogiccondRecord
     */
    public TCoreLogiccondRecord() {
        super(TCoreLogiccond.T_CORE_LOGICCOND);
    }

    /**
     * Create a detached, initialised TCoreLogiccondRecord
     */
    public TCoreLogiccondRecord(Integer pkCoreLogiccond, Integer fkAndgrp, Integer fkVariable, Integer depth, BigDecimal logiccondValue, Integer fkVariable2, Integer depth2, String logiccondComparator, Integer compareVariables, LocalDateTime lastchange) {
        super(TCoreLogiccond.T_CORE_LOGICCOND);

        setPkCoreLogiccond(pkCoreLogiccond);
        setFkAndgrp(fkAndgrp);
        setFkVariable(fkVariable);
        setDepth(depth);
        setLogiccondValue(logiccondValue);
        setFkVariable2(fkVariable2);
        setDepth2(depth2);
        setLogiccondComparator(logiccondComparator);
        setCompareVariables(compareVariables);
        setLastchange(lastchange);
        resetChangedOnNotNull();
    }
}
