/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VCoreElementTreeRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreElementTree extends TableImpl<VCoreElementTreeRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_core_element_tree</code>
     */
    public static final VCoreElementTree V_CORE_ELEMENT_TREE = new VCoreElementTree();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VCoreElementTreeRecord> getRecordType() {
        return VCoreElementTreeRecord.class;
    }

    /**
     * The column <code>common_dd.v_core_element_tree.key_element</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer> KEY_ELEMENT = createField(DSL.name("key_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.key_element_type</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer> KEY_ELEMENT_TYPE = createField(DSL.name("key_element_type"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.key_parent_element</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer> KEY_PARENT_ELEMENT = createField(DSL.name("key_parent_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.dn</code>.
     */
    public final TableField<VCoreElementTreeRecord, String> DN = createField(DSL.name("dn"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.order_nr</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer> ORDER_NR = createField(DSL.name("order_nr"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.short_name</code>.
     */
    public final TableField<VCoreElementTreeRecord, String> SHORT_NAME = createField(DSL.name("short_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.is_public</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer> IS_PUBLIC = createField(DSL.name("is_public"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.hier_lang</code>.
     */
    public final TableField<VCoreElementTreeRecord, String> HIER_LANG = createField(DSL.name("hier_lang"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.tree</code>.
     */
    public final TableField<VCoreElementTreeRecord, String> TREE = createField(DSL.name("tree"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.path</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer[]> PATH = createField(DSL.name("path"), SQLDataType.INTEGER.array(), this, "");

    /**
     * The column <code>common_dd.v_core_element_tree.stage</code>.
     */
    public final TableField<VCoreElementTreeRecord, Integer> STAGE = createField(DSL.name("stage"), SQLDataType.INTEGER, this, "");

    private VCoreElementTree(Name alias, Table<VCoreElementTreeRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VCoreElementTree(Name alias, Table<VCoreElementTreeRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_core_element_tree" as  WITH RECURSIVE q(key_element, key_element_type, key_parent_element, dn, order_nr, short_name, stage, is_public, hier_lang, tree) AS (
                SELECT t_core_element_hierarchy.key_element,
                   t_core_element_hierarchy.key_element_type,
                   t_core_element_hierarchy.key_parent_element,
                   t_core_element_hierarchy.dn,
                   t_core_element_hierarchy.order_nr,
                   t_core_element_hierarchy.short_name,
                   t_core_element_hierarchy.stage,
                   t_core_element_hierarchy.is_public,
                   t_core_element_hierarchy.hier_lang,
                   t_core_element_hierarchy.short_name,
                   ARRAY[t_core_element_hierarchy.order_nr] AS path
                  FROM common_dd.t_core_element_hierarchy
                 WHERE (t_core_element_hierarchy.key_parent_element IS NULL)
               UNION ALL
                SELECT h.key_element,
                   h.key_element_type,
                   h.key_parent_element,
                   h.dn,
                   h.order_nr,
                   h.short_name,
                   h.stage,
                   h.is_public,
                   h.hier_lang,
                   ((q_1.tree || '.'::text) || h.short_name) AS tree,
                   (q_1.path || h.order_nr)
                  FROM (common_dd.t_core_element_hierarchy h
                    JOIN q q_1 ON ((q_1.key_element = h.key_parent_element)))
               )
        SELECT key_element,
           key_element_type,
           key_parent_element,
           dn,
           order_nr,
           short_name,
           is_public,
           hier_lang,
           tree,
           path,
           stage
          FROM q;
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_core_element_tree</code> table
     * reference
     */
    public VCoreElementTree(String alias) {
        this(DSL.name(alias), V_CORE_ELEMENT_TREE);
    }

    /**
     * Create an aliased <code>common_dd.v_core_element_tree</code> table
     * reference
     */
    public VCoreElementTree(Name alias) {
        this(alias, V_CORE_ELEMENT_TREE);
    }

    /**
     * Create a <code>common_dd.v_core_element_tree</code> table reference
     */
    public VCoreElementTree() {
        this(DSL.name("v_core_element_tree"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VCoreElementTree as(String alias) {
        return new VCoreElementTree(DSL.name(alias), this);
    }

    @Override
    public VCoreElementTree as(Name alias) {
        return new VCoreElementTree(alias, this);
    }

    @Override
    public VCoreElementTree as(Table<?> alias) {
        return new VCoreElementTree(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreElementTree rename(String name) {
        return new VCoreElementTree(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreElementTree rename(Name name) {
        return new VCoreElementTree(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreElementTree rename(Table<?> name) {
        return new VCoreElementTree(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreElementTree where(Condition condition) {
        return new VCoreElementTree(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreElementTree where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreElementTree where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreElementTree where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreElementTree where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreElementTree where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreElementTree where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreElementTree where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreElementTree whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreElementTree whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
