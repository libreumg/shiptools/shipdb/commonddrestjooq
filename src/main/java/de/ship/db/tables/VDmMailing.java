/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VDmMailingRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDmMailing extends TableImpl<VDmMailingRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>common_dd.v_dm_mailing</code>
     */
    public static final VDmMailing V_DM_MAILING = new VDmMailing();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VDmMailingRecord> getRecordType() {
        return VDmMailingRecord.class;
    }

    /**
     * The column <code>common_dd.v_dm_mailing.fk_dm_person</code>.
     */
    public final TableField<VDmMailingRecord, Integer> FK_DM_PERSON = createField(DSL.name("fk_dm_person"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.tree</code>.
     */
    public final TableField<VDmMailingRecord, String> TREE = createField(DSL.name("tree"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.dn</code>.
     */
    public final TableField<VDmMailingRecord, String> DN = createField(DSL.name("dn"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.key_element</code>.
     */
    public final TableField<VDmMailingRecord, Integer> KEY_ELEMENT = createField(DSL.name("key_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.key_element_type</code>.
     */
    public final TableField<VDmMailingRecord, Integer> KEY_ELEMENT_TYPE = createField(DSL.name("key_element_type"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.varname</code>.
     */
    public final TableField<VDmMailingRecord, String> VARNAME = createField(DSL.name("varname"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.var_long_ger</code>.
     */
    public final TableField<VDmMailingRecord, String> VAR_LONG_GER = createField(DSL.name("var_long_ger"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.fk_usnr</code>.
     */
    public final TableField<VDmMailingRecord, Integer> FK_USNR = createField(DSL.name("fk_usnr"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.user_mail</code>.
     */
    public final TableField<VDmMailingRecord, String> USER_MAIL = createField(DSL.name("user_mail"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.recip_id</code>.
     */
    public final TableField<VDmMailingRecord, Integer> RECIP_ID = createField(DSL.name("recip_id"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_dm_mailing.value</code>.
     */
    public final TableField<VDmMailingRecord, String> VALUE = createField(DSL.name("value"), SQLDataType.CLOB, this, "");

    private VDmMailing(Name alias, Table<VDmMailingRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VDmMailing(Name alias, Table<VDmMailingRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_dm_mailing" as  SELECT ml.fk_dm_person,
          eh.tree,
          eh.dn,
          eh.key_element,
          eh.key_element_type,
          eh.short_name AS varname,
          l1.value AS var_long_ger,
          ps.fk_usnr,
          ps.user_mail,
          rc.pk AS recip_id,
          ln.value
         FROM ((((((common_dd.t_dm_maillist ml
           LEFT JOIN common_dd.v_core_element_tree eh ON ((eh.key_element = ml.fk_element)))
           LEFT JOIN common_dd.t_core_translation l1 ON ((l1.fk_trans_key = eh.hier_lang)))
           LEFT JOIN common_dd.t_dm_person ps ON ((ps.pk = ml.fk_dm_person)))
           LEFT JOIN common_dd.t_dm_recipient rc ON ((rc.pk = ml.fk_maillist_recip)))
           LEFT JOIN common_dd.t_core_translation_key lak ON ((lak.translation_key = rc.fk_lang)))
           LEFT JOIN common_dd.t_core_translation ln ON ((ln.fk_trans_key = lak.translation_key)))
        WHERE ((l1.isocode = 'de_DE'::common_dd.enum_isocode) AND (l1.category = 'crf'::common_dd.enum_trans_category) AND (ln.isocode = 'de_DE'::common_dd.enum_isocode) AND (ln.category = 'crf'::common_dd.enum_trans_category));
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_dm_mailing</code> table reference
     */
    public VDmMailing(String alias) {
        this(DSL.name(alias), V_DM_MAILING);
    }

    /**
     * Create an aliased <code>common_dd.v_dm_mailing</code> table reference
     */
    public VDmMailing(Name alias) {
        this(alias, V_DM_MAILING);
    }

    /**
     * Create a <code>common_dd.v_dm_mailing</code> table reference
     */
    public VDmMailing() {
        this(DSL.name("v_dm_mailing"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VDmMailing as(String alias) {
        return new VDmMailing(DSL.name(alias), this);
    }

    @Override
    public VDmMailing as(Name alias) {
        return new VDmMailing(alias, this);
    }

    @Override
    public VDmMailing as(Table<?> alias) {
        return new VDmMailing(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VDmMailing rename(String name) {
        return new VDmMailing(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDmMailing rename(Name name) {
        return new VDmMailing(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDmMailing rename(Table<?> name) {
        return new VDmMailing(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmMailing where(Condition condition) {
        return new VDmMailing(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmMailing where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmMailing where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmMailing where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmMailing where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmMailing where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmMailing where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VDmMailing where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmMailing whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VDmMailing whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
