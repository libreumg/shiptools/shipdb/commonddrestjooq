/*
 * This file is generated by jOOQ.
 */
package de.ship.db.tables;


import de.ship.db.CommonDd;
import de.ship.db.tables.records.VCoreAnnotationMaelstromRecord;

import java.util.Collection;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Name;
import org.jooq.PlainSQL;
import org.jooq.QueryPart;
import org.jooq.SQL;
import org.jooq.Schema;
import org.jooq.Select;
import org.jooq.Stringly;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCoreAnnotationMaelstrom extends TableImpl<VCoreAnnotationMaelstromRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of
     * <code>common_dd.v_core_annotation_maelstrom</code>
     */
    public static final VCoreAnnotationMaelstrom V_CORE_ANNOTATION_MAELSTROM = new VCoreAnnotationMaelstrom();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VCoreAnnotationMaelstromRecord> getRecordType() {
        return VCoreAnnotationMaelstromRecord.class;
    }

    /**
     * The column
     * <code>common_dd.v_core_annotation_maelstrom.unique_name</code>.
     */
    public final TableField<VCoreAnnotationMaelstromRecord, String> UNIQUE_NAME = createField(DSL.name("unique_name"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_annotation_maelstrom.fk_element</code>.
     */
    public final TableField<VCoreAnnotationMaelstromRecord, Integer> FK_ELEMENT = createField(DSL.name("fk_element"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>common_dd.v_core_annotation_maelstrom.taxonomy</code>.
     */
    public final TableField<VCoreAnnotationMaelstromRecord, String> TAXONOMY = createField(DSL.name("taxonomy"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_annotation_maelstrom.vocabulary</code>.
     */
    public final TableField<VCoreAnnotationMaelstromRecord, String> VOCABULARY = createField(DSL.name("vocabulary"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>common_dd.v_core_annotation_maelstrom.term</code>.
     */
    public final TableField<VCoreAnnotationMaelstromRecord, String> TERM = createField(DSL.name("term"), SQLDataType.CLOB, this, "");

    private VCoreAnnotationMaelstrom(Name alias, Table<VCoreAnnotationMaelstromRecord> aliased) {
        this(alias, aliased, (Field<?>[]) null, null);
    }

    private VCoreAnnotationMaelstrom(Name alias, Table<VCoreAnnotationMaelstromRecord> aliased, Field<?>[] parameters, Condition where) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view("""
        create view "v_core_annotation_maelstrom" as  SELECT e.unique_name,
          a.fk_element,
          split_part(a.key, '::'::text, 1) AS taxonomy,
          split_part(a.key, '::'::text, 2) AS vocabulary,
          a.value AS term
         FROM (common_dd.t_core_annotation a
           LEFT JOIN common_dd.t_core_element_hierarchy e ON ((e.key_element = a.fk_element)))
        WHERE (a.source = 'maelstrom'::common_dd.enum_annotationsource);
        """), where);
    }

    /**
     * Create an aliased <code>common_dd.v_core_annotation_maelstrom</code>
     * table reference
     */
    public VCoreAnnotationMaelstrom(String alias) {
        this(DSL.name(alias), V_CORE_ANNOTATION_MAELSTROM);
    }

    /**
     * Create an aliased <code>common_dd.v_core_annotation_maelstrom</code>
     * table reference
     */
    public VCoreAnnotationMaelstrom(Name alias) {
        this(alias, V_CORE_ANNOTATION_MAELSTROM);
    }

    /**
     * Create a <code>common_dd.v_core_annotation_maelstrom</code> table
     * reference
     */
    public VCoreAnnotationMaelstrom() {
        this(DSL.name("v_core_annotation_maelstrom"), null);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : CommonDd.COMMON_DD;
    }

    @Override
    public VCoreAnnotationMaelstrom as(String alias) {
        return new VCoreAnnotationMaelstrom(DSL.name(alias), this);
    }

    @Override
    public VCoreAnnotationMaelstrom as(Name alias) {
        return new VCoreAnnotationMaelstrom(alias, this);
    }

    @Override
    public VCoreAnnotationMaelstrom as(Table<?> alias) {
        return new VCoreAnnotationMaelstrom(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreAnnotationMaelstrom rename(String name) {
        return new VCoreAnnotationMaelstrom(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreAnnotationMaelstrom rename(Name name) {
        return new VCoreAnnotationMaelstrom(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public VCoreAnnotationMaelstrom rename(Table<?> name) {
        return new VCoreAnnotationMaelstrom(name.getQualifiedName(), null);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreAnnotationMaelstrom where(Condition condition) {
        return new VCoreAnnotationMaelstrom(getQualifiedName(), aliased() ? this : null, null, condition);
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreAnnotationMaelstrom where(Collection<? extends Condition> conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreAnnotationMaelstrom where(Condition... conditions) {
        return where(DSL.and(conditions));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreAnnotationMaelstrom where(Field<Boolean> condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreAnnotationMaelstrom where(SQL condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreAnnotationMaelstrom where(@Stringly.SQL String condition) {
        return where(DSL.condition(condition));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreAnnotationMaelstrom where(@Stringly.SQL String condition, Object... binds) {
        return where(DSL.condition(condition, binds));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    @PlainSQL
    public VCoreAnnotationMaelstrom where(@Stringly.SQL String condition, QueryPart... parts) {
        return where(DSL.condition(condition, parts));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreAnnotationMaelstrom whereExists(Select<?> select) {
        return where(DSL.exists(select));
    }

    /**
     * Create an inline derived table from this table
     */
    @Override
    public VCoreAnnotationMaelstrom whereNotExists(Select<?> select) {
        return where(DSL.notExists(select));
    }
}
